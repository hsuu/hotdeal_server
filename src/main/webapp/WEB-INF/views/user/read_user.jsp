<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType = "text/html;charset=utf-8" %>
<html>
<head>
	<meta charset="UTF-8">
	<title>AdminLTE | Dashboard</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
</head>
<body class="skin-black">
<!-- header logo: style can be found in header.less -->

<%@include file="../header.jsp"%>
<div class="wrapper row-offcanvas row-offcanvas-left">
	<%@include file="../sidemenu.jsp"%>
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				유저 상세 보기
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">Dashboard</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">

			<!-- Small boxes (Stat box) -->
			<div class="row">
				<table class="table">
					<tr class="active">
						<td width="20%">회원 번호</td>
						<td width="80%" id="user_id">${userVO.user_id}</td>
					</tr>
					<tr>
						<td width="20%">회원 이메일</td>
						<td width="80%">${userVO.user_email}</td>
					</tr>
					<tr>
						<td width="20%">회원 이름</td>
						<td width="80%">${userVO.user_name}</td>
					</tr>
					<tr>
						<td width="20%">회원 연락처</td>
						<td width="80%">${userVO.user_phone}</td>
					</tr>
					<tr>
						<td width="20%">회원 생일</td>
						<td width="80%" id="user_birth">${userVO.user_birth}</td>
					</tr>
					<tr>
						<td width="20%">회원 성별</td>
						<td width="80%">${userVO.user_gender}</td>
					</tr>
					<tr>
						<td width="20%">회원 가입일</td>
						<td width="80%"><fmt:formatDate value="${userVO.user_in_date}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
					</tr>
					<%--<tr>--%>
						<%--<td width="20%">sms 수신 여부</td>--%>

						<%--<td width="80%">--%>
							<%--<c:set var="sms" value="${userVO.user_sms}" />--%>
							<%--<c:when test="${sms eq 1}">동의</c:when>--%>
							<%--<c:otherwise>거부</c:otherwise>--%>
						<%--</td>--%>
					<%--</tr>--%>
					<%--<tr>--%>
						<%--<td width="20%">마케팅 수신 여부</td>--%>
						<%--<td width="80%">--%>
							<%--<c:set var="marketing" value="${userVO.user_marketing}" />--%>
							<%--<c:when test="${marketing eq 1}">동의</c:when>--%>
							<%--<c:otherwise>거부</c:otherwise>--%>
						<%--</td>--%>
					<%--</tr>--%>
					<tr>
						<td width="20%">비밀번호 생년월일로 변경</td>
						<td width="80%"><button type="button" class="btn btn-primary" id="btn_password">변경</button></td>
					</tr>

					<c:if test="${userVO.user_artist eq 0}">
					<tr>
						<td width="20%">아티스트 설정</td>
						<td width="80%"><button type="button" class="btn btn-info" id="btn_artist">설정</button></td>
					</tr>
					</c:if>
				</table>
			</div>
			<!-- /.row -->

		</section><!-- /.content -->
	</aside><!-- /.right-side -->
</div><!-- ./wrapper -->

<script>
        $("#btn_artist").on("click",function(){
            var msg="아티스트로 설정하시겠습니까?";
            if(confirm(msg)!=0){
                var url='/admin/user/artist/'+$('#user_id').text();
                $.ajax({
                    url:url,
                    type:'GET',
                    success:function(data){
                        data=data.trim();
                        if(data==="ok"){
                            alert("성공");
                            location.reload();
                        }else{
                            alert("error");
                        }
                    },
                    error:function(error){
                        alert("error");
                    }
                });
			}else{

			}
        });
        $("#btn_password").on("click",function(){
            var msg="패스워드를 생년월일로 변경하시겠습니까?";
            if(confirm(msg)!=0){
                var url='/admin/user/password?user_id='+$('#user_id').text()+'&user_birth='+$('#user_birth').text();
                $.ajax({
                    url:url,
                    type:'GET',
                    success:function(data){
                        data=data.trim();
                        if(data==="ok"){
                            alert("성공");
                            location.reload();
						}else{
                            alert("error");
						}
                    },
                    error:function(error){
                        alert("error");
                    }
                });
            }else{

            }
        });

</script>
</body>
</html>