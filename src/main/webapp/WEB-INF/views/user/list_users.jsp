<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType = "text/html;charset=utf-8" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>AdminLTE | Dashboard</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
</head>
<style>
	#artist{
		color:red;
		font-weight: bold;
	}
</style>
<body class="skin-black">
<%@include file="../header.jsp"%>
<div class="wrapper row-offcanvas row-offcanvas-left">
	<%@include file="../sidemenu.jsp"%>

	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				회원 목록
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">Dashboard</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">

			<!-- Small boxes (Stat box) -->
			<div class="col-md-12">
				<input type="text" id="input_name"/>
				<button type="button" id="btn_find">검색</button>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>회원 번호</th>
							<th>회원 이메일</th>
							<th>회원 이름</th>
							<th>회원 연락처</th>
							<th>회원 생일</th>
							<th>회원 성별</th>
							<th>회원 가입일</th>
						</tr>
					</thead>

					<tbody>

					<c:forEach items="${list}" var="list">
						<tr>
							<td><a href="/admin/user/${list.user_id}">${list.user_id}</a><c:if test="${list.user_artist eq 1}">&nbsp;<span id="artist">[artist]</span></artist></c:if></td>
							<td>${list.user_email}</td>
							<td>${list.user_name}</td>
							<td>${list.user_phone}</td>
							<td>${list.user_birth}</td>
							<td>${list.user_gender}</td>
							<td><fmt:formatDate value="${list.user_in_date}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
						</tr>
					</c:forEach>

					</tbody>

				</table>

				<jsp:include page="../paging.jsp" flush="true">
					<jsp:param name="firstPageNo" value="${paging.firstPageNo}" />
					<jsp:param name="prevPageNo" value="${paging.prevPageNo}" />
					<jsp:param name="startPageNo" value="${paging.startPageNo}" />
					<jsp:param name="pageNo" value="${paging.pageNo}" />
					<jsp:param name="endPageNo" value="${paging.endPageNo}" />
					<jsp:param name="nextPageNo" value="${paging.nextPageNo}" />
					<jsp:param name="finalPageNo" value="${paging.finalPageNo}" />
				</jsp:include>

			</div><!-- /.row -->

		</section><!-- /.content -->
	</aside><!-- /.right-side -->
</div><!-- ./wrapper -->
<script>
	    $('#btn_find').on('click',function(){
	        var name=$('#input_name').val();
	        location.href='/admin/user?name='+name;
		});
</script>

</body>
</html>