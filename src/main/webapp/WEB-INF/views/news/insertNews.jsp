<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType = "text/html;charset=utf-8" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>AdminLTE | Dashboard</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
</head>
<body class="skin-black">
<!-- header logo: style can be found in header.less -->
<%@include file="../header.jsp"%>
<div class="wrapper row-offcanvas row-offcanvas-left">
	<%@include file="../sidemenu.jsp"%>

	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				소식 등록
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">Dashboard</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">

			<!-- Small boxes (Stat box) -->
			<div class="row">
				<form id="frm" action="/admin/news/" enctype="multipart/form-data" method="post" onsubmit="return change_content()">
					<table class="table">
						<tr>
							<td width="15%">제목</td>
							<td width="*"><input type="text" id="frm_title" name="title"/></td>
						</tr>
						<tr>
							<td width="15%">내용</td>
							<td><textarea rows="20" cols="100" title="내용" id="frm_contents" name="content"></textarea></td>
						</tr>
						<tr>
							<td width="15%">메인 이미지</td>
							<td width="*">
								<input type="file" name="main_img"/><br>
							</td>
						</tr>

					</table>

					<button type="submit" class="btn btn-block btn-default">작성하기</button>
				</form>


			</div><!-- /.row -->

		</section><!-- /.content -->
	</aside><!-- /.right-side -->
</div><!-- ./wrapper -->
<script>
    function change_content() {
        $('#frm_contents').val().replace(/\n/g, "<br>");
        return true;
    }
</script>
</body>
</html>