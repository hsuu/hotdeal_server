<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType = "text/html;charset=utf-8" %>
<html>
<head>
	<meta charset="UTF-8">
	<title>AdminLTE | Dashboard</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
</head>
<body class="skin-black">
<!-- header logo: style can be found in header.less -->

<%@include file="../header.jsp"%>
<div class="wrapper row-offcanvas row-offcanvas-left">
	<%@include file="../sidemenu.jsp"%>
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				소식 상세 보기
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">Dashboard</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">
			<!-- Small boxes (Stat box) -->
			<div class="row">
				<a href="/admin/news/edit/${newsVO.news_id}" class="btn btn-primary">수정</a>
				<a href="/admin/news/del/${newsVO.news_id}" class="btn btn-danger">삭제</a>
				<table class="table">
					<tr class="active">
						<td width="20%">소식 번호</td>
						<td width="80%" id="item_id">${newsVO.news_id}</td>
					</tr>
					<tr>
						<td width="20%">소식 제목</td>
						<td width="80%">${newsVO.news_title}</td>
					</tr>
					<tr>
						<td width="20%">소식 내용</td>
						<td width="80%" id="content"><pre>${newsVO.news_content}</pre></td>
					</tr>
					<tr>
						<td width="20%">소식 이미지</td>
						<td width="80%"><img style="width:100%" src="/newsimg/${newsVO.news_id}.png"></td>
					</tr>
					<tr>
						<td width="20%">작성일</td>
						<td width="80%"><fmt:formatDate value="${newsVO.news_indate}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
					</tr>

				</table>
			</div>
			<!-- /.row -->

		</section><!-- /.content -->
	</aside><!-- /.right-side -->
</div><!-- ./wrapper -->
</body>
</html>