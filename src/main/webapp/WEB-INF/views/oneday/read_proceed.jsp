<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType = "text/html;charset=utf-8" %>
<html>
<head>
	<meta charset="UTF-8">
	<title>AdminLTE | Dashboard</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
</head>
<body class="skin-black">
<!-- header logo: style can be found in header.less -->

<%@include file="../header.jsp"%>
<div class="wrapper row-offcanvas row-offcanvas-left">
	<%@include file="../sidemenu.jsp"%>
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				원데이클래스 상세 보기
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">Dashboard</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">

			<!-- Small boxes (Stat box) -->
			<div class="row">
				<table class="table">
					<tr class="active">
						<td width="20%">아이템 번호</td>
						<td width="80%">${onedayVO.oneday_id}</td>
					</tr>
					<tr>
						<td width="20%">제목</td>
						<td width="80%">${onedayVO.oneday_title}</td>
					</tr>
					<tr>
						<td width="20%">내용</td>
						<td width="80%">${onedayVO.oneday_content}</td>
					</tr>
					<tr>
						<td width="20%">작성일</td>
						<td width="80%"><fmt:formatDate value="${onedayVO.oneday_in_date}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
					</tr>
					<tr>
						<td width="20%">경매 시작일</td>
						<td width="80%"><fmt:formatDate value="${onedayVO.oneday_start_date}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
					</tr>
					<tr class="danger">
						<td width="20%">경매 종료일</td>
						<td width="80%"><fmt:formatDate value="${onedayVO.oneday_end_date}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
					</tr>
					<tr>
						<td width="20%">가격</td>
						<td width="80%">${onedayVO.oneday_price}</td>
					</tr>
					<tr>
						<td width="20%">이미지 보기</td>
						<td width="80%"><a href="#" class="img_tag" value="${onedayVO.oneday_main_img}">메인 이미지</a>
										<a href="#" class="img_tag" value="${onedayVO.oneday_sub_img}">서브 이미지</a></td>
					</tr>
					<c:forEach items="${onedayVO.odyOptionVOList}" var="list">
						<tr class="success">
							<td width="20%">옵션 번호(구매 목록 보기)</td>
							<td width="80%"><a href="#" class="option_tag">${list.ody_option_id}</a></td>
						</tr>
						<tr>
							<td width="20%">옵션 이름</td>
							<td width="80%">${list.ody_option_name}</td>
						</tr>
						<tr>
							<td width="20%">옵션 인원</td>
							<td width="80%">${list.ody_option_num}</td>
						</tr>
					</c:forEach>

				</table>
			</div>
			<!-- /.row -->

		</section><!-- /.content -->
	</aside><!-- /.right-side -->
</div><!-- ./wrapper -->

<script>
        $(".img_tag").on("click",function(){
            var url=$(this).attr('value');
            window.open(url,"이미지 보기",'location=no');
        });
        $(".option_tag").on("click",function(){
            var url="/admin/oneday/option/"+$(this).text();
            window.open(url,"구매 명단 보기",'location=no');
        });
</script>
</body>
</html>