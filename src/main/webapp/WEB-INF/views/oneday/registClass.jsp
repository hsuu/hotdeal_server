<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType = "text/html;charset=utf-8" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>AdminLTE | Dashboard</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
</head>
<body class="skin-black">
<!-- header logo: style can be found in header.less -->
<%@include file="../header.jsp"%>
<div class="wrapper row-offcanvas row-offcanvas-left">
	<%@include file="../sidemenu.jsp"%>

	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				원데이 클래스 등록
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">Dashboard</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">

			<!-- Small boxes (Stat box) -->
			<div class="row">
				<form id="frm" enctype="multipart/form-data" method="post">
					<table class="table">
						<tr>
							<td width="15%">제목</td>
							<td width="*"><input type="text" id="frm_title" name="title"/></td>
						</tr>
						<tr>
							<td width="15%">시작일</td>
							<td width="*">
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</div>
									<input type="text" class="form-control" id="start_date" name="start_date" />
								</div>
							</td>
						</tr>
						<tr>
							<td width="15%">종료일</td>
							<td width="*">
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</div>
									<input type="text" class="form-control datepicker" id="end_date" name="end_date"/>
								</div>
							</td>
						</tr>
						<tr>
							<td width="15%">가격</td>
							<td width="*">
								<input type="number" class="input" id="frm_min" name="price"/>
							</td>
						</tr>
						<tr>
							<td colspan="2"><textarea rows="20" cols="100" title="내용" id="frm_contents" name="content"></textarea></td>
						</tr>
						<tr>
							<td width="15%">메인 이미지</td>
							<td width="*">
								<input type="file" name="main_img"/><br>
							</td>
						</tr>
						<tr>
							<td width="15%">서브 이미지</td>
							<td width="*">
								<input type="file" name="sub_img"/><br>
							</td>
						</tr>
						<tr>
							<td width="15%">항목 이름</td>
							<td><input type="text" name="option_name"/></td>
						</tr>
						<tr>
							<td width="15%">인원</td>
							<td><input type="number" name="max_num"></td>
						</tr>
						<tr id="tr_option">
							<td width="15%">옵션</td>
							<td><button type="button" class="btn btn-primary" id="addOption">추가</button></td>
						</tr>
					</table>


					<button type="button" class="btn btn-block btn-default" id="upload">작성하기</button>
				</form>

				<!--form script-->
				<script type="text/javascript">

				</script>

			</div><!-- /.row -->

		</section><!-- /.content -->
	</aside><!-- /.right-side -->
</div><!-- ./wrapper -->


<script>

	$("#start_date").appendDtpicker({
        dateFormat:'yyyy-MM-DD hh:mm',
        locale:'ko',
        closeOnSelected: true
    });
    $("#end_date").appendDtpicker({
        dateFormat:'yyyy-MM-DD hh:mm',
        locale:'ko',
        closeOnSelected: true
    });

	$("#addOption").on("click",function(){
	   	$("#tr_option").after("<tr><td width='15%'>항목 이름</td><td><input type='text' name='option_name'/></td>"+
           "</tr><tr><td width='15%'>인원</td><td><input type='number' name='max_num'></td></tr>");
	});

	$("#upload").on("click",function(){
	    $(this).prop("disabled",true);
	    var formData=new FormData($("#frm")[0]);
	    $.ajax({
			url:'',
			data:formData,
			dataType:'json',
			processData:false,
			contentType:false,
			type:'POST',
			success:function(data){
			},
			error:function(error){

			    if(error.status==200){
			        alert("성공");
			        window.location.href="home";
				}
			}
		});
	});
</script>
<!-- add new calendar event modal -->

</body>
</html>