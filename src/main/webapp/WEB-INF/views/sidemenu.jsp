<%@ page contentType = "text/html;charset=utf-8" %>
<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li>

                <a href="/admin/home">
                    <i class="fa fa-dashboard"></i> <span>home</span>
                </a>
            </li>
            <li>
                <a href="/admin/logout">
                    <i class="fa fa-dashboard"></i> <span>logout</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-bar-chart-o"></i>
                    <span>핫딜</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/admin/hotdeal"><i class="fa fa-angle-double-right"></i>핫딜 등록</a></li>
                    <li><a href="/admin/hotdeal/proceed"><i class="fa fa-angle-double-right"></i> 진행 중 핫딜</a></li>
                    <li><a href="/admin/hotdeal/soon"><i class="fa fa-angle-double-right"></i> 진행 예정 핫딜</a></li>
                    <li><a href="/admin/hotdeal/bidend"><i class="fa fa-angle-double-right"></i> 입찰 종료된 핫딜</a></li>
                    <li><a href="/admin/hotdeal/ready"><i class="fa fa-angle-double-right"></i>결제 대기 핫딜</a></li>
                    <li><a href="/admin/hotdeal/end"><i class="fa fa-angle-double-right"></i> 완료된 핫딜</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-bar-chart-o"></i>
                    <span>원데이클래스</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/admin/oneday"><i class="fa fa-angle-double-right"></i>원데이클래스 등록</a></li>
                    <li><a href="/admin/oneday/proceed"><i class="fa fa-angle-double-right"></i>진행 중 원데이클래스 </a></li>
                    <li><a href="/admin/oneday/soon"><i class="fa fa-angle-double-right"></i>진행 예정 원데이클래스 </a></li>
                    <li><a href="/admin/oneday/end"><i class="fa fa-angle-double-right"></i>완료된 원데이클래스 </a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-bar-chart-o"></i>
                    <span>티켓픽스</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/admin/ticket"><i class="fa fa-angle-double-right"></i>티켓픽스 등록</a></li>
                    <li><a href="/admin/ticket/proceed"><i class="fa fa-angle-double-right"></i>진행 중 티켓픽스 </a></li>
                    <li><a href="/admin/ticket/soon"><i class="fa fa-angle-double-right"></i>진행 예정 티켓픽스 </a></li>
                    <li><a href="/admin/ticket/end"><i class="fa fa-angle-double-right"></i>완료된 티켓픽스 </a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-bar-chart-o"></i>
                    <span>유저 관리</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/admin/users"><i class="fa fa-angle-double-right"></i>유저 목록</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-bar-chart-o"></i>
                    <span>질문 관리</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/admin/inquiry/"><i class="fa fa-angle-double-right"></i>질문 목록</a></li>
                    <li><a href="/admin/inquiry/done"><i class="fa fa-angle-double-right"></i>답변한 질문 목록</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-bar-chart-o"></i>
                    <span>공지사항 관리</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/admin/notice/"><i class="fa fa-angle-double-right"></i>공지사항 등록</a></li>
                    <li><a href="/admin/notice/list"><i class="fa fa-angle-double-right"></i>공지사항 목록</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-bar-chart-o"></i>
                    <span>픽스미 소식 관리</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/admin/news/"><i class="fa fa-angle-double-right"></i>소식 등록</a></li>
                    <li><a href="/admin/news/list"><i class="fa fa-angle-double-right"></i>소식 목록</a></li>
                </ul>
            </li>
            <li>

            <a href="/admin/sms">
                <i class="fa fa-dashboard"></i> <span>문자보내기</span>
            </a>
        </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>