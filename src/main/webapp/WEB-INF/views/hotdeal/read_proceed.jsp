<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType = "text/html;charset=utf-8" %>
<html>
<head>
	<meta charset="UTF-8">
	<title>AdminLTE | Dashboard</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
</head>
<body class="skin-black">
<!-- header logo: style can be found in header.less -->

<%@include file="../header.jsp"%>
<div class="wrapper row-offcanvas row-offcanvas-left">
	<%@include file="../sidemenu.jsp"%>
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				핫딜 상세 보기
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">Dashboard</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">

			<!-- Small boxes (Stat box) -->
			<div class="row">
				<button type="button" class="btn btn-danger" id="btn_end">핫딜 조기종료</button><br>
				<table class="table">
					<tr class="active">
						<td width="20%">아이템 번호</td>
						<td width="80%" id="item_id">${itemVO.item_id}</td>
					</tr>
					<tr>
						<td width="20%">제목</td>
						<td width="80%">${itemVO.item_title}</td>
					</tr>
					<tr>
						<td width="20%">내용</td>
						<td width="80%">${itemVO.item_content}</td>
					</tr>
					<tr>
						<td width="20%">작성일</td>
						<td width="80%"><fmt:formatDate value="${itemVO.item_in_date}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
					</tr>
					<tr>
						<td width="20%">경매 시작일</td>
						<td width="80%"><fmt:formatDate value="${itemVO.item_start_date}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
					</tr>
					<tr class="danger">
						<td width="20%">경매 종료일</td>
						<td width="80%"><fmt:formatDate value="${itemVO.item_end_date}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
					</tr>
					<tr>
						<td width="20%">최고가</td>
						<td width="80%">${itemVO.item_max_price}</td>
					</tr>
					<tr>
						<td width="20%">최저가</td>
						<td width="80%">${itemVO.item_min_price}</td>
					</tr>
					<tr>
						<td width="20%">이미지 보기</td>
						<td width="80%"><a href="#" class="img_tag" value="${itemVO.item_main_img}">메인 이미지</a>
										<a href="#" class="img_tag" value="${itemVO.item_sub_img}">서브 이미지</a>
							<a href="#" class="img_tag" value="${itemVO.item_link_img}">링크 이미지</a></td>
					</tr>
					<tr>
						<td width="20%">링크</td>
						<td width="80%">${itemVO.item_link}</td>
					</tr>
					<c:forEach items="${itemVO.optionVOList}" var="list">
						<tr class="success">
							<td width="20%">옵션 번호 (입찰 명단 보기)</td>
							<td width="80%"><a href="#" value="${list.option_min_num}" class="option_tag">${list.option_id}</a></td>
						</tr>
						<tr>
							<td width="20%">옵션 이름</td>
							<td width="80%">${list.option_name}</td>
						</tr>
						<tr>
							<td width="20%">옵션 최소 인원 / 현재 입찰수</td>
							<td width="80%">${list.option_min_num} / ${list.bid_count}</td>
						</tr>
						<tr>
						</tr>
					</c:forEach>

				</table>
			</div>
			<!-- /.row -->

		</section><!-- /.content -->
	</aside><!-- /.right-side -->
</div><!-- ./wrapper -->

<script>
        $(".img_tag").on("click",function(){
            var url=$(this).attr('value');
            window.open(url,"이미지 보기",'location=no');
        });
        $(".option_tag").on("click",function(){
            var url="/admin/hotdeal/option/"+$(this).text();
            var min_num=$(this).attr('value');
            window.open(url+"?num="+min_num,"입찰 명단 보기",'location=no');
        });
        $("#btn_end").on("click",function(){
            var msg="조기 종료 하시겠습니까?";
            if(confirm(msg)!=0){
                location.href='/admin/hotdeal/finish/'+$('#item_id').text();
			}else{

			}
        });
</script>
</body>
</html>