<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType = "text/html;charset=utf-8" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>AdminLTE | Dashboard</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
</head>
<body class="skin-black">
<%@include file="../header.jsp"%>
<div class="wrapper row-offcanvas row-offcanvas-left">
	<%@include file="../sidemenu.jsp"%>

	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				완료된 핫딜
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">Dashboard</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">

			<!-- Small boxes (Stat box) -->
			<div class="col-md-12">

				<table class="table table-striped">
					<thead>
						<tr>
							<th width="10%">핫딜 번호</th>
							<th width="60%">핫딜 제목</th>
							<th width="15%">시작일</th>
							<th width="15%">종료일</th>
						</tr>
					</thead>

					<tbody>

					<c:forEach items="${list}" var="list">
						<tr>
							<td>${list.item_id}</td>
							<td><a href="/admin/hotdeal/read/proceed?item_id=${list.item_id}">${list.item_title}</a></td>
							<td><fmt:formatDate value="${list.item_start_date}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
							<td><fmt:formatDate value="${list.item_end_date}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
						</tr>
					</c:forEach>

					</tbody>

				</table>

			</div><!-- /.row -->

		</section><!-- /.content -->
	</aside><!-- /.right-side -->
</div><!-- ./wrapper -->


</body>
</html>