<%@ page import="com.alwayke.lessonap.domain.BidVO" %>
<%@ page import="java.util.List" %>
<%@ page import="com.alwayke.lessonap.domain.PaymentVO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType = "text/html;charset=utf-8" %>
<!-- bootstrap 3.0.2 -->
<link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<html>
<body>
    <table class="table table-bordered">
            <tr>
                <th>번호</th>
                <th>유저 아이디</th>
                <th>유저 이메일</th>
                <th>유저 이름</th>
                <th>유저 연락처</th>
                <th>입찰 가격</th>
                <th>입찰 시간</th>
                <th>결제 아이디(입금 목록 조회)</th>
            </tr>
        <%
            int min_num=Integer.parseInt(request.getParameter("num"));
            List<PaymentVO> list=(List<PaymentVO>)request.getAttribute("list");
            int i=1;
            for(PaymentVO vo:list){
        %>
            <tr<%if(i==min_num){%> class="danger"<%}%>>
                <td><%=i%></td>
                <td><a href="#"><%=vo.getUser_id()%></a></td>
                <td><%=vo.getUser_email()%></td>
                <td><%=vo.getUser_name()%></td>
                <td><%=vo.getUser_phone()%></td>
                <td><%=vo.getBid_price()%></td>
                <td><%=vo.getBid_in_date()%></td>
                <%  if(vo.getBid_id()>0) {
                        out.print("<td>"+vo.getPayment_id()+"</td>");
                    }else { %>
                <td><a href="" class="check_payment" value="<%=vo.getUser_name()%>">조회</a><input type="hidden" id="bid_id" value="<%=vo.getBidding_bid_id()%>"/> </td>
              <%}%>
            </tr>
        <%i++;
            }%>
    </table>
</body>
<script>
$('.check_payment').on('click',function () {
    var url="/admin/payment?user_name="+$(this).attr('value')+"&bid_id="+$(this).siblings("#bid_id").val();
    window.open(url,'입금 목록 조회','location=no,height=800,width=1200');
});

</script>
</html>
