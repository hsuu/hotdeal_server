<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType = "text/html;charset=utf-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>AdminLTE | Dashboard</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
</head>
<body class="skin-black">
<%@include file="../header.jsp"%>
<div class="wrapper row-offcanvas row-offcanvas-left">
	<%@include file="../sidemenu.jsp"%>

	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				진행중 티켓픽스
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">Dashboard</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">

			<!-- Small boxes (Stat box) -->
			<div class="col-md-12">

				<table class="table table-striped">
					<thead>
						<tr>
							<th width="10%">티켓픽스 번호</th>
							<th width="60%">티켓픽스 제목</th>
							<th width="15%">시작일</th>
							<th width="15%">종료일</th>
						</tr>
					</thead>

					<tbody>

					<c:forEach items="${list}" var="list">
						<tr>
							<td>${list.ticket_id}</td>
							<td><a href="/admin/ticket/read/proceed?ticket_id=${list.ticket_id}">${list.ticket_title}</a></td>
							<td><fmt:formatDate value="${list.ticket_start_date}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
							<td><fmt:formatDate value="${list.ticket_end_date}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
						</tr>
					</c:forEach>

					</tbody>

				</table>

			</div><!-- /.row -->

		</section><!-- /.content -->
	</aside><!-- /.right-side -->
</div><!-- ./wrapper -->


</body>
</html>