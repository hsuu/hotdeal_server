<%@ page import="com.alwayke.lessonap.domain.BidVO" %>
<%@ page import="java.util.List" %>
<%@ page import="com.alwayke.lessonap.domain.PaymentVO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType = "text/html;charset=utf-8" %>
<!-- bootstrap 3.0.2 -->
<link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<html>
<head>
    <title>입찰자</title>
</head>
<body>
    <table class="table table-bordered">
            <tr>
                <th>결제 번호</th>
                <th>입금주</th>
                <th>입금 은행</th>
                <th>입금액</th>
                <th>입금 시간</th>
                <th>입금 확인</th>
            </tr>
        <%
            int bid_id=Integer.parseInt(request.getParameter("bid_id"));
            out.print("<input type='hidden' id='bid_id' value='"+bid_id+"'/>");
            List<PaymentVO> list=(List<PaymentVO>)request.getAttribute("list");
            for(PaymentVO vo:list){
        %>
            <tr>
                <td class="payment_id"><%=vo.getPayment_id()%></td>
                <td><%=vo.getUser_name()%></td>
                <td><%=vo.getPayment_bankcode()%></td>
                <td><%=vo.getPayment_price()%></td>
                <td><%=vo.getPayment_paydt()%></td>
                <td><a href="#" class="check_deposit">입금 확인</a></td>
            </tr>
        <%
            }%>
    </table>
</body>
<script>
$('.check_deposit').on('click',function () {
    var info='{'+
        '"bid_id":'+$('#bid_id').val()+
        ',"payment_id":'+$(this).parent().siblings('.payment_id').text()
        +'}';

    $.ajax({
        headers: {
            'Content-Type': 'application/json'
        },
        url:'/admin/payment/',
        data:info,
        type:'POST',
        success:function(data){

            if(data=="ok"){
                alert("성공");
                opener.parent.location.reload();
                self.close();
            }else{

                alert("실패");
            }
        },
        error:function(error){
            alert("네트워크 실패");
        }
    });
});

</script>
</html>
