<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>AdminLTE | Dashboard</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
</head>
<body class="skin-black">
<%@include file="header.jsp" %>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <%@include file="sidemenu.jsp" %>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                문자보내기
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Small boxes (Stat box) -->
            <div class="col-md-12">

                <form method="post" action="/admin/sms" onsubmit="return pubByteCheckTextarea()">
                    <h1>메세지</h1>
                    <textarea id="message" name="message" cols="25" rows="4"></textarea>
                    <p id="counter"></p>
                    <button type="submit">전송</button>
                </form>

            </div><!-- /.row -->

        </section><!-- /.content -->
    </aside><!-- /.right-side -->
</div><!-- ./wrapper -->
<script>

    function checkByte(str) {
        var byteNum = 0;
        var byteTxt = "";
        for (i = 0; i < str.length; i++) {
            byteNum += (str.charCodeAt(i) > 127) ? 2 : 1;
            if (byteNum < 500) {
                byteTxt += str.charAt(i);
            }
        }
        return Math.round(byteNum / 2);
    }

    $("#message").keydown(function () {

        $("#counter").text(checkByte($(this).val()));
    });

    function pubByteCheckTextarea() {

        var size = checkByte($("#message").val());
        if (size > 45) {
            alert("45자 이상 입력할수 없습니다.");
            return false;
        } else {
            return true;
        }
    };

</script>

</body>
</html>


