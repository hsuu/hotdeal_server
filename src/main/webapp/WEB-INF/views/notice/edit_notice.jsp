<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType = "text/html;charset=utf-8" %>
<html>
<head>
	<meta charset="UTF-8">
	<title>AdminLTE | Dashboard</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
</head>
<body class="skin-black">
<!-- header logo: style can be found in header.less -->

<%@include file="../header.jsp"%>
<div class="wrapper row-offcanvas row-offcanvas-left">
	<%@include file="../sidemenu.jsp"%>
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				공지사항 수정
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">Dashboard</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">

			<!-- Small boxes (Stat box) -->
			<div class="row">
					<form id="frm" action="/admin/notice/edit/${noticeVO.notice_id}" method="post">
						<button type="submit" class="btn btn-primary">수정 확인</button>
				<table class="table">

					<tr class="active">
						<td width="20%">공지사항 번호</td>
						<td width="80%" id="item_id">${noticeVO.notice_id}</td>
					</tr>
					<tr>
						<td width="20%">공지사항 제목</td>
						<td width="80%"><input type="text" id="frm_title" name="title" value="${noticeVO.notice_title}"/></td>
					</tr>
					<tr>
						<td width="20%">공지사항 내용</td>
						<td width="80%" id="content"><textarea rows="20" cols="100" title="내용" id="frm_contents" name="content">${noticeVO.notice_content}</textarea></td>
					</tr>
					<tr>
						<td width="20%">작성일</td>
						<td width="80%"><fmt:formatDate value="${noticeVO.notice_indate}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
					</tr>


				</table>

					</form>
			</div>
			<!-- /.row -->

		</section><!-- /.content -->
	</aside><!-- /.right-side -->
</div><!-- ./wrapper -->
</body>
</html>