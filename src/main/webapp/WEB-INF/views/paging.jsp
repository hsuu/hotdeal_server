<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="pages text-center">
    <ul class="pagination">
        <li><a href="javascript:goPage(${param.firstPageNo})"><i class="fa fa-angle-double-left"></i></a></li>
        <li><a href="javascript:goPage(${param.prevPageNo})"><i class="fa fa-angle-left"></i></a></li>

        <c:forEach var="i" begin="${param.startPageNo}" end="${param.endPageNo}" step="1">
            <c:choose>
                <c:when test="${i eq param.pageNo}"><li class="active"><a href="javascript:goPage(${i})">${i}</a></li></c:when>
                <c:otherwise><li><a href="javascript:goPage(${i})">${i}</a></li></c:otherwise>
            </c:choose>
        </c:forEach>
        <li><a href="javascript:goPage(${param.nextPageNo})"><i class="fa fa-angle-right"></i></a></li>
        <li><a href="javascript:goPage(${param.finalPageNo})"><i class="fa fa-angle-double-right"></i></a></li>
    </ul>
</div>

<script>
    function goPage(num){

        document.location.href="users?page="+num;
    }

</script>