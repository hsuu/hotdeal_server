<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType = "text/html;charset=utf-8" %>
<html>
<head>
	<meta charset="UTF-8">
	<title>AdminLTE | Dashboard</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
</head>
<body class="skin-black">
<!-- header logo: style can be found in header.less -->
<%@include file="../header.jsp"%>

<div class="wrapper row-offcanvas row-offcanvas-left">
	<%@include file="../sidemenu.jsp"%>
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				질문 상세 보기
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">Dashboard</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">

			<!-- Small boxes (Stat box) -->
			<div class="row">
				<table class="table">
					<tr class="active">
						<td width="20%">질문 번호</td>
						<td width="80%">${inquiry.inquiry_id}</td>
					</tr>
					<tr>
						<td width="20%">유저 아이디</td>
						<td width="80%">${inquiry.user_id}</td>
					</tr>
					<tr>
						<td width="20%">유저 이메일</td>
						<td width="80%">${inquiry.user_email}</td>
					</tr>
					<tr>
						<td width="20%">제목</td>
						<td width="80%">${inquiry.inquiry_title}</td>
					</tr>
					<tr>
						<td width="20%">질문 내용</td>
						<td width="80%"><pre>${inquiry.inquiry_content}</pre></td>
					</tr>
					<tr class="danger">
						<td width="20%">게시일</td>
						<td width="80%"><fmt:formatDate value="${inquiry.inquiry_indate}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
					</tr>
					<c:if test="${inquiry.answer_id != 0}">
						<tr>
							<td width="20%">답변 번호</td>
							<td width="80%">${inquiry.answer_id}</td>
						</tr>
						<tr>
							<td width="20%">답변 내용</td>
							<td width="80%"><pre>${inquiry.inquiry_answer}</pre></td>
						</tr>
						<tr>
							<td width="20%">답변일</td>
							<td width="80%"><fmt:formatDate value="${inquiry.answer_date}" pattern="yyyy-MM-dd HH:mm:ss" /></td>

						</tr>
					</c:if>

				</table>
				<c:if test="${inquiry.answer_id == 0}">
					<form method="post" action="">
					<textarea name="inquiry_answer" rows="10" cols="50"></textarea>
					<button type="submit" id="btn_answer">답변 하기</button>
					</form>
				</c:if>
			</div>
			<!-- /.row -->

		</section><!-- /.content -->
	</aside><!-- /.right-side -->
</div><!-- ./wrapper -->

<!-- add new calendar event modal -->

<script>
        $(".img_tag").on("click",function(){
            var url=$(this).attr('value');
            window.open(url,"이미지 보기",'location=no');
        });

</script>
</body>
</html>