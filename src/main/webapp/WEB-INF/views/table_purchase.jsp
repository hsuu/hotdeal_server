<%@ page import="com.alwayke.lessonap.domain.BidVO" %>
<%@ page import="java.util.List" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType = "text/html;charset=utf-8" %>
<!-- bootstrap 3.0.2 -->
<link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<html>
<head>
    <title>입찰자</title>
</head>
<body>
    <table class="table table-bordered">
            <tr>
                <th>번호</th>
                <th>유저 아이디</th>
                <th>유저 이메일</th>
                <th>구매 시간</th>
            </tr>
        <%
            int min_num=Integer.parseInt(request.getParameter("num"));
            List<BidVO> list=(List<BidVO>)request.getAttribute("list");
            int i=1;
            for(BidVO vo:list){
        %>
            <tr<%if(i==min_num){%> class="danger"<%}%>>
                <td><%=i%></td>
                <td><%=vo.getUser_id()%></td>
                <td><%=vo.getUser_email()%></td>
                <td><%=vo.getBid_price()%></td>
                <td><%=vo.getBid_in_date()%></td>
            </tr>
        <%i++;
            }%>
    </table>
</body>
</html>
