<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
    <title>
        픽스미
    </title>
    <meta name="description" content="세계 최초 교육형 레슨 공동구매 서비스 픽스미 FIXME"/>
    <meta name="naver-site-verification" content="51e5e2536ea0c452e696aeaf3d747a22ba90843e"/>
    <meta property="og:type" content="website">
    <meta property="og:title" content="픽스미">
    <meta property="og:description" content="세계 최초 교육형 레슨 공동구매 서비스 픽스미 FIXME">
    <meta property="og:image" content="http://www.fixme.kr/img/home/home.jpg">
    <meta property="og:url" content="http://www.fixme.kr">
 
</head>
   <link rel="shortcut icon" href="/path/to/favicon.ico">

<script type="text/javascript">
 var host = location.host.toLowerCase();
  var currentAddress = location.href;
   if (host.indexOf("www")== -1) { 
   currentAddress = currentAddress.replace("//","//www."); 
   location.href = currentAddress; 
} 
   </script>
<!-- jQuery 2.0.2 -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<body style="margin:0;padding:0">
    <div style="max-width:100%">

<img id="main_img" src="" style="width: 100%; height: auto;">
    </div>
<script>
$(function() {
    console.log($(window).width());
    if($(window).width()<=320){
$("#main_img").attr("src","img/home/home_mobile1.jpg");
    }else if($(window).width()>320&&$(window).width()<=480){
      $("#main_img").attr("src","img/home/home_hdpi.jpg");
      
   }else if($(window).width()>480&&$(window).width()<=1280){
      $("#main_img").attr("src","img/home/home_xhdpi.jpg");
      
   }else if($(window).width()>1280&&$(window).width()<=1334){
  $("#main_img").attr("src","img/home/home_mobile2.jpg");
//    }
//    else if($(window).width()>1334&&$(window).width()<=1920){
//       $("#main_img").attr("src","img/home/home_xxhdpi.jpg");
      
   }else{
      $("#main_img").attr("src","img/home/home.jpg");
   }
});
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-96761016-2', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
