<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
    <title>Home</title>
    <script src="https://code.jquery.com/jquery-2.2.1.min.js"></script>
</head>
<body>

<a href="/admin/home">관리자 페이지로..</a>

<form method="post" action="/admin/login">
    <h1>이메일 :</h1>
    <input type="text" id="username" name="username"/>
    <br>
    <h1>비밀번호:</h1>
    <input type="password" id="password" name="password"/>
    <button type="submit" id="request_access">로그인</button>
</form>
<h1> data : </h1>
<input type="hidden" id="access_token"/>
<br><br>
<input type="hidden" id="refresh_token"/>
<p id="response_jwt"></p>


<button type="button" id="request_item">profile</button>

<h1>로그인한 유저의 정보 : </h1>
<p id="response_item"></p>
<script>
   // var urls="http://120.142.183.202:8181";

//   var urls="";
//
//    function refresh(){
//
//        console.log("\n refreshing token...............\n")
//        $.ajax({
//            // 전송할 Data를 Post방식으로 전송합니다.
//            type : "POST",
//            // URL 주소를 설정합니다.
//            url : urls+"/oauth/token",
//            headers:{
//                Accept:"application/json"
//            },
//            // 전송할 Data를 zoneId, isOver의 이름으로 넘겨줍니다.
//            data : {
//                "grant_type" : "refresh_token", "refresh_token" : $("#refresh_token").val()
//            },
//            beforeSend: function (xhr) {
//                xhr.setRequestHeader('Authorization', 'Basic bXktdHJ1c3RlZC1jbGllbnQ6Y2xpZW50X3NlY3JldA==');
//            },
//            // Data를 성공적으로 전송할 경우에 대한 Code 입니다.
//            success : function (data) {
//                if(data.error==null){
//                    console.log(data);
//                    $("#response_jwt").text("access token : "+data.access_token+"\n refresh_token : "+data.refresh_token);
//                    $("#access_token").val(data.access_token);
//                    $("#refresh_token").val(data.refresh_token);x
//                }else{
//                    $("#response_jwt").text("error!");
//                }
//
//            },
//            error : function(request, status, error ) {   // 오류가 발생했을 때 호출된다.
//
//                console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
//
//                console.log("로그인 화면으로")
//
//            },
//
//            complete : function () {   // 정상이든 비정상인든 실행이 완료될 경우 실행될 함수
//
//            }
//
//
//        });
//    }
//
//
//    $("#request_item").click(function () {
//        $.ajax({
//            // 전송할 Data를 Post방식으로 전송합니다.
//            type : "GET",
//            // URL 주소를 설정합니다.
//            url : urls+"/profile/",
//            headers:{
//                Accept:"application/json"
//            },
//            // 전송할 Data를 zoneId, isOver의 이름으로 넘겨줍니다.
//            data : {
//                "access_token" : $("#access_token").val()
//            },
//            // Data를 성공적으로 전송할 경우에 대한 Code 입니다.
//            success : function (data) {
//                    $("#response_item").text(JSON.stringify(data));
//
//            },
//            error : function(request, status, error ) {   // 오류가 발생했을 때 호출된다.
//
//                if(request.status==401){
//
//                    var error_json=JSON.parse(request.responseText);
//                console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
//
//               if(error_json.error_description.match('Access token expired:')){
//
//                    refresh();
//                }else{
//                    console.log("로그인 화면으로......");
//                }
//
//                }else{
//                    console.log("잘못된 요청");
//                    $("#response_item").text();
//                }
//
//
//            },
//
//            complete : function () {   // 정상이든 비정상인든 실행이 완료될 경우 실행될 함수
//
//            }
//
//
//        });
//    });
//
//    $("#request_access").click(function () {
//
//
//        console.log("data : "+   "grant_type" +"password, username :" + $("#username").val()+", password : "+$("#password").val());
//        $.ajax({
//            // 전송할 Data를 Post방식으로 전송합니다.
//            type : "POST",
//            // URL 주소를 설정합니다.
//            url : urls+"/oauth/token",
//            // 전송할 Data를 zoneId, isOver의 이름으로 넘겨줍니다.
//            data : {
//                "grant_type" : "password", "username" : $("#username").val(),"password":$("#password").val()
//            },
//            beforeSend: function (xhr) {
//                xhr.setRequestHeader('Accept','application/json');
//                xhr.setRequestHeader('Authorization', 'Basic bXktdHJ1c3RlZC1jbGllbnQ6Y2xpZW50X3NlY3JldA==');
//            },
//            // Data를 성공적으로 전송할 경우에 대한 Code 입니다.
//            success : function (data) {
//                if(data.error==null){
//                    console.log(data);
//                    $("#response_jwt").text("access token : "+data.access_token+"\n refresh_token : "+data.refresh_token);
//                    $("#access_token").val(data.access_token);
//                    $("#refresh_token").val(data.refresh_token);
//                }else{
//                    $("#response_jwt").text("error!");
//                }
//
//            },
//            error : function(request, status, error ) {   // 오류가 발생했을 때 호출된다.
//
//                console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
//
//            },
//
//            complete : function () {   // 정상이든 비정상인든 실행이 완료될 경우 실행될 함수
//
//            }
//
//
//        });
//    })

</script>
</body>
</html>
