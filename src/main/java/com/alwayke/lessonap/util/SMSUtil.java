package com.alwayke.lessonap.util;


import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by hs on 2017-05-25.
 */
@Component
public class SMSUtil {

    public int generateRandomNumber(){
        int randInt=0;
        Random random=new Random();
        randInt=random.nextInt(1000000)+1000000;
        if(randInt>1000000){
            randInt-=1000000;
        }
        return randInt;
    }

    public String sendCertificationSMS(String phone){
        String url="http://serviceok.munjaok.com:9010/WEDIS/alwayke/mdinf.asp";
        String finalResult="";
        try {



        HttpClient client= HttpClientBuilder.create().build();
        HttpPost post=new HttpPost(url);

        List<NameValuePair> urlParameters = new ArrayList<>();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar cal=Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.MINUTE,-2);

        int randInt=generateRandomNumber();

        urlParameters.add(new BasicNameValuePair("VIEW", "0"));
        urlParameters.add(new BasicNameValuePair("OPER", "sendMsg"));
        urlParameters.add(new BasicNameValuePair("MDID", "alwayke"));
        urlParameters.add(new BasicNameValuePair("MDPW", "b73e61daabc63f7b6ba2c111d6d9ad15"));
        //송신 전화번호
        urlParameters.add(new BasicNameValuePair("MDSENDPHONE", "01033988283"));
        urlParameters.add(new BasicNameValuePair("MDRCVCNT", Integer.toString(1)));
        urlParameters.add(new BasicNameValuePair("MDRCVPHONE", phone));
        urlParameters.add(new BasicNameValuePair("MDSENDTYPE", "S"));
        urlParameters.add(new BasicNameValuePair("MDSENDMSG", "픽스미 인증번호는 ["+Integer.toString(randInt)+"] 입니다. "));
        urlParameters.add(new BasicNameValuePair("MDSENDDATE",dateFormat.format(cal.getTime())));

        post.setEntity(new UrlEncodedFormEntity(urlParameters, StandardCharsets.UTF_8));

        HttpResponse response = client.execute(post);
        System.out.println("Response Code : "
                + response.getStatusLine().getStatusCode());

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        String[] lineResult=rd.readLine().split("\\|");
        if (lineResult[0].equals("00")) {
            finalResult=Integer.toString(randInt);
        } else if (lineResult[0].equals("80")) {
            finalResult= "error";
        }

        }catch (Exception e){
            e.printStackTrace();
        }
        return finalResult;
    }

    public String sendEmail(String phone,String email){
        String url="http://serviceok.munjaok.com:9010/WEDIS/alwayke/mdinf.asp";
        String finalResult="";
        try {



            HttpClient client= HttpClientBuilder.create().build();
            HttpPost post=new HttpPost(url);

            List<NameValuePair> urlParameters = new ArrayList<>();
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Calendar cal=Calendar.getInstance();
            cal.setTime(new Date());
            cal.add(Calendar.MINUTE,-2);

            int randInt=generateRandomNumber();

            urlParameters.add(new BasicNameValuePair("VIEW", "0"));
            urlParameters.add(new BasicNameValuePair("OPER", "sendMsg"));
            urlParameters.add(new BasicNameValuePair("MDID", "alwayke"));
            urlParameters.add(new BasicNameValuePair("MDPW", "b73e61daabc63f7b6ba2c111d6d9ad15"));
            //송신 전화번호
            urlParameters.add(new BasicNameValuePair("MDSENDPHONE", "01033988283"));
            urlParameters.add(new BasicNameValuePair("MDRCVCNT", Integer.toString(1)));
            urlParameters.add(new BasicNameValuePair("MDRCVPHONE", phone));
            urlParameters.add(new BasicNameValuePair("MDSENDTYPE", "S"));
            urlParameters.add(new BasicNameValuePair("MDSENDMSG", "픽스미 회원님의 이메일은 "+email+"입니다. "));
            urlParameters.add(new BasicNameValuePair("MDSENDDATE",dateFormat.format(cal.getTime())));

            post.setEntity(new UrlEncodedFormEntity(urlParameters, StandardCharsets.UTF_8));

            HttpResponse response = client.execute(post);
            System.out.println("Response Code : "
                    + response.getStatusLine().getStatusCode());

            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));

            String[] lineResult=rd.readLine().split("\\|");
            if (lineResult[0].equals("00")) {
                finalResult=Integer.toString(randInt);
            } else if (lineResult[0].equals("80")) {
                finalResult= "error";
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return finalResult;
    }
    public String sendEmailPassword(String phone){
        String url="http://serviceok.munjaok.com:9010/WEDIS/alwayke/mdinf.asp";
        String finalResult="";
        try {



            HttpClient client= HttpClientBuilder.create().build();
            HttpPost post=new HttpPost(url);

            List<NameValuePair> urlParameters = new ArrayList<>();
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Calendar cal=Calendar.getInstance();
            cal.setTime(new Date());
            cal.add(Calendar.MINUTE,-2);

            int randInt=generateRandomNumber();

            urlParameters.add(new BasicNameValuePair("VIEW", "0"));
            urlParameters.add(new BasicNameValuePair("OPER", "sendMsg"));
            urlParameters.add(new BasicNameValuePair("MDID", "alwayke"));
            urlParameters.add(new BasicNameValuePair("MDPW", "b73e61daabc63f7b6ba2c111d6d9ad15"));
            //송신 전화번호
            urlParameters.add(new BasicNameValuePair("MDSENDPHONE", "01033988283"));
            urlParameters.add(new BasicNameValuePair("MDRCVCNT", Integer.toString(1)));
            urlParameters.add(new BasicNameValuePair("MDRCVPHONE", phone));
            urlParameters.add(new BasicNameValuePair("MDSENDTYPE", "S"));
            urlParameters.add(new BasicNameValuePair("MDSENDMSG", "픽스미 회원님의 비밀번호가 생년월일로 변경 되었습니다."));
            urlParameters.add(new BasicNameValuePair("MDSENDDATE",dateFormat.format(cal.getTime())));

            post.setEntity(new UrlEncodedFormEntity(urlParameters, StandardCharsets.UTF_8));

            HttpResponse response = client.execute(post);
            System.out.println("Response Code : "
                    + response.getStatusLine().getStatusCode());

            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));

            String[] lineResult=rd.readLine().split("\\|");
            if (lineResult[0].equals("00")) {
                finalResult=Integer.toString(randInt);
            } else if (lineResult[0].equals("80")) {
                finalResult= "error";
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return finalResult;
    }

    public String convertListToString(ArrayList<String> list){
        String result="";
        for(String s:list){
            result+=(s+"|");
        }
        result=result.substring(0,result.length()-1);

        return result;
    }


    public String sendSMS(ArrayList<String> phoneList,String message){
        String url="http://serviceok.munjaok.com:9010/WEDIS/alwayke/mdinf.asp";
        String finalResult="";
        try {

            String phone=convertListToString(phoneList);

            HttpClient client= HttpClientBuilder.create().build();
            HttpPost post=new HttpPost(url);

            List<NameValuePair> urlParameters = new ArrayList<>();
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Calendar cal=Calendar.getInstance();
            cal.setTime(new Date());
            cal.add(Calendar.MINUTE,-2);

            int randInt=generateRandomNumber();

            urlParameters.add(new BasicNameValuePair("VIEW", "0"));
            urlParameters.add(new BasicNameValuePair("OPER", "sendMsg"));
            urlParameters.add(new BasicNameValuePair("MDID", "alwayke"));
            urlParameters.add(new BasicNameValuePair("MDPW", "b73e61daabc63f7b6ba2c111d6d9ad15"));
            //송신 전화번호
            urlParameters.add(new BasicNameValuePair("MDSENDPHONE", "01033988283"));
            urlParameters.add(new BasicNameValuePair("MDRCVCNT", Integer.toString(phoneList.size())));
            urlParameters.add(new BasicNameValuePair("MDRCVPHONE", phone));
            urlParameters.add(new BasicNameValuePair("MDSENDTYPE", "S"));
            urlParameters.add(new BasicNameValuePair("MDSENDMSG", message));
            urlParameters.add(new BasicNameValuePair("MDSENDDATE",dateFormat.format(cal.getTime())));

            post.setEntity(new UrlEncodedFormEntity(urlParameters, StandardCharsets.UTF_8));

            HttpResponse response = client.execute(post);
            System.out.println("Response Code : "
                    + response.getStatusLine().getStatusCode());

            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));

            String[] lineResult=rd.readLine().split("\\|");
            if (lineResult[0].equals("00")) {
                finalResult=Integer.toString(randInt);
            } else if (lineResult[0].equals("80")) {
                finalResult= "error";
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return finalResult;
    }

}
