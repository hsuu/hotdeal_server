package com.alwayke.lessonap.util;


import com.fasterxml.jackson.core.JsonEncoding;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.context.annotation.Bean;
import org.springframework.security.jwt.Jwt;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;

import java.util.Base64;

@Component
public class JwtUtil {

    private static final Base64.Decoder decoder = Base64.getDecoder();

    public int getUserID(String jwt) throws Exception{
        JSONParser jsonParser=new JSONParser();
        JSONObject obj=(JSONObject)jsonParser.parse(decodePayload(jwt));

        return Integer.parseInt(obj.get("user_name").toString());
    }

    public String decodePayload(String jwt) throws Exception{
            String[] array=jwt.split("\\.");



        return new String(decoder.decode(array[1].getBytes()));
    }
}
