package com.alwayke.lessonap.controller;

import com.alwayke.lessonap.domain.*;
import com.alwayke.lessonap.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.Iterator;

/**
 * Created by hs on 2017-05-29.
 */

@Controller
@RequestMapping("/admin/news")
public class AdminNewsController {

    @Autowired
    private NewsService service;

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public String insertNewsPage(Model model){
        return "news/insertNews";
    }

    @Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.REPEATABLE_READ,rollbackFor = RuntimeException.class)
    @RequestMapping(value = "/",method = RequestMethod.POST)
    public String insertNews(MultipartHttpServletRequest request){
        String returnPage="home";
        try {
            int news_id=service.insertNews(new NewsVO(request.getParameter("title"),request.getParameter("content")));
            //이미지 업로드
//			String path=request.getSession().getServletContext().getRealPath("/")+"/newsimg/";
            String path="C://newsimg/";
            File dir=new File(path);
            if(!dir.isDirectory()){
                dir.mkdir();
            }
            Iterator<String> files=request.getFileNames();
            while (files.hasNext()){
                MultipartFile mFile = request.getFile(files.next());

                mFile.transferTo(new File(path+news_id+".png"));

            }

        }catch (Exception e){
            e.printStackTrace();
            returnPage="error";
        }
        return returnPage;
    }
    @RequestMapping(value = "/list",method = RequestMethod.GET)
    public String listNews(Model model){
        try {
            model.addAttribute("list",service.listNews());
        }catch (Exception e){
            e.printStackTrace();
        }
        return "news/list_news";
    }

    @RequestMapping(value = "/{news_id}",method = RequestMethod.GET)
    public String readNews(Model model,@PathVariable("news_id")int news_id){
        try {
            model.addAttribute(service.readNews(news_id));
        }catch (Exception e){
            e.printStackTrace();
        }
        return "news/read_news";
    }
    @RequestMapping(value = "/edit/{news_id}",method = RequestMethod.GET)
    public String editNewsPage(Model model, @PathVariable("news_id")int news_id){
        try {
            model.addAttribute(service.readNews(news_id));
        }catch (Exception e){
            e.printStackTrace();
        }
        return "news/edit_news";
    }
    @RequestMapping(value = "/edit/{news_id}",method = RequestMethod.POST)
    public String editNews(@PathVariable("news_id")int news_id,HttpServletRequest request){
        String returnPage="home";
        try {
            service.updateNews(new NewsVO(news_id,request.getParameter("title"),request.getParameter("content")));
        }catch (Exception e){
            e.printStackTrace();
            returnPage="error";
        }
        return returnPage;
    }

    @RequestMapping(value = "/del/{news_id}",method = RequestMethod.GET)
    public String deleteNews(@PathVariable("news_id")int news_id){
        String returnPage="home";
        try {
            service.deleteNews(news_id);
        }catch (Exception e){
            e.printStackTrace();
            returnPage="error";
        }
        return returnPage;
    }
}
