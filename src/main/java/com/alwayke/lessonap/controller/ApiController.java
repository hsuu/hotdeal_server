package com.alwayke.lessonap.controller;

import com.alwayke.lessonap.domain.DepositVO;
import com.alwayke.lessonap.service.AdminService;
import com.alwayke.lessonap.util.SMSUtil;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by hs on 2017-05-15.
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/")
public class ApiController {

    @Autowired
    private SMSUtil smsUtil;
    @Autowired
    private AdminService service;

    // 입금확인
    @RequestMapping(value = "/deposit", method = RequestMethod.GET)
    public String deposit(@RequestParam("tid") String tid,@RequestParam("phone_number") String phone_number,@RequestParam("bankcode") int bankcode,
                          @RequestParam("account") String account,@RequestParam("price") int price,@RequestParam("name") String name,
                          @RequestParam("paydt") String paydt,@RequestParam("uid") String uid,@RequestParam("charset") String charset,
                          @RequestParam("speed") int speed) {
        DepositVO vo=new DepositVO(tid,phone_number,bankcode,account,price,name,paydt,uid,charset,speed);

        String result="fail";

        HttpServletRequest req=((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        String ip=req.getHeader("X-FORWARDED-FOR");
        if (ip == null || "".equals(ip)) {
            ip = req.getRemoteAddr();
        }
        if(ip.equals("182.163.234.6")||ip.equals("115.71.237.204")||ip.equals("106.250.175.106")||ip.equals("192.168.0.1")){
            try {
                result=service.deposit(vo);
            }catch (Exception e){
                e.printStackTrace();
            }


        }else{
            result="fail";
        }

        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/cert/{phone}",method = RequestMethod.GET)
    public String certificatePhone(Model model,@PathVariable("phone")String phone) {
        String finalResult=smsUtil.sendCertificationSMS(phone);

        return finalResult;
    }
}
