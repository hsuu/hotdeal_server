package com.alwayke.lessonap.controller;

import com.alwayke.lessonap.domain.*;
import com.alwayke.lessonap.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by hs on 2017-05-29.
 */

@Controller
@RequestMapping("/admin/notice")
public class AdminNoticeController {

    @Autowired
    private NoticeService service;

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public String insertNoticePage(Model model){
        return "notice/insertNotice";
    }

    @RequestMapping(value = "/",method = RequestMethod.POST)
    public String insertNotice(HttpServletRequest request){
        NoticeVO vo=new NoticeVO(request.getParameter("title"),request.getParameter("content"));
        String returnPage="home";
        try {
                service.insertNotice(vo);
        }catch (Exception e){
            e.printStackTrace();
            returnPage="error";
        }
        return returnPage;
    }
    @RequestMapping(value = "/list",method = RequestMethod.GET)
    public String listNotice(Model model){
        try {
            model.addAttribute("list",service.listNotice());
        }catch (Exception e){
            e.printStackTrace();
        }
        return "notice/list_notice";
    }

    @RequestMapping(value = "/{notice_id}",method = RequestMethod.GET)
    public String readNotice(Model model,@PathVariable("notice_id")int notice_id){
        try {
            model.addAttribute(service.readNotice(notice_id));
        }catch (Exception e){
            e.printStackTrace();
        }
        return "notice/read_notice";
    }
    @RequestMapping(value = "/edit/{notice_id}",method = RequestMethod.GET)
    public String editNoticePage(Model model, @PathVariable("notice_id")int notice_id){
        try {
            model.addAttribute(service.readNotice(notice_id));
        }catch (Exception e){
            e.printStackTrace();
        }
        return "notice/edit_notice";
    }
    @RequestMapping(value = "/edit/{notice_id}",method = RequestMethod.POST)
    public String editNotice(Model model, @PathVariable("notice_id")int notice_id,HttpServletRequest request){
        String returnPage="home";
        try {
            service.updateNotice(new NoticeVO(notice_id,request.getParameter("title"),request.getParameter("content")));
        }catch (Exception e){
            e.printStackTrace();
            returnPage="error";
        }
        return returnPage;
    }

    @RequestMapping(value = "/del/{notice_id}",method = RequestMethod.GET)
    public String deleteNotice(Model model,@PathVariable("notice_id")int notice_id){
        String returnPage="home";
        try {
            service.deleteNotice(notice_id);
        }catch (Exception e){
            e.printStackTrace();
            returnPage="error";
        }
        return returnPage;
    }

}
