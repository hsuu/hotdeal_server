package com.alwayke.lessonap.controller;

import com.alwayke.lessonap.domain.NewsVO;
import com.alwayke.lessonap.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

/**
 * Created by hs on 2017-05-29.
 */

@RestController
@RequestMapping("/news")
public class NewsController {

    @Autowired
    private NewsService service;

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public ResponseEntity<List<NewsVO>> listNews(){
        ResponseEntity<List<NewsVO>> entity=null;
        try {
            entity=new ResponseEntity<>(service.listNews(), HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            entity=new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return entity;
    }

    @RequestMapping(value = "/{news_id}",method = RequestMethod.GET)
    public ResponseEntity<NewsVO> readNews(@PathVariable("news_id")int news_id){
        ResponseEntity<NewsVO> entity=null;
        try {
            entity=new ResponseEntity<>(service.readNews(news_id),HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            entity = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return entity;
    }
}
