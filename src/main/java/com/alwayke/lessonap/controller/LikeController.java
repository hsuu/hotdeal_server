package com.alwayke.lessonap.controller;

import com.alwayke.lessonap.domain.LikeVO;
import com.alwayke.lessonap.service.ItemService;
import com.alwayke.lessonap.service.LikeService;
import com.alwayke.lessonap.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by hs on 2017-06-28.
 */
@RestController
@RequestMapping("/like")
public class LikeController {

    @Autowired
    private LikeService service;
    @Autowired
    private JwtUtil jwtUtil;

    @RequestMapping(value = "/{item_id}",method = RequestMethod.GET)
    public ResponseEntity<String> likeItem(@RequestParam("access_token")String access_token, @PathVariable("item_id")int item_id){
        ResponseEntity<String> entity=null;
        try {
            int result=service.likeItem(new LikeVO(item_id,jwtUtil.getUserID(access_token)));
            if(result==1){//좋아요 지움
                entity=new ResponseEntity<String>("0", HttpStatus.OK);
            }else{//좋아요 추가
                entity=new ResponseEntity<String>("1", HttpStatus.OK);
            }

        }catch (Exception e){
            e.printStackTrace();
            entity=new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
        }
        return entity;
    }

}
