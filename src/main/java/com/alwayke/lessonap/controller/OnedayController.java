package com.alwayke.lessonap.controller;

import com.alwayke.lessonap.domain.ItemVO;
import com.alwayke.lessonap.domain.OdyPurchaseVO;
import com.alwayke.lessonap.domain.OnedayVO;
import com.alwayke.lessonap.domain.TicPurchaseVO;
import com.alwayke.lessonap.service.ItemService;
import com.alwayke.lessonap.service.OnedayService;
import com.alwayke.lessonap.util.JwtUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/oneday")
public class OnedayController {
	private static final JwtUtil jwtUtil=new JwtUtil();

	@Inject
	private OnedayService service;
	private static final Logger logger = LoggerFactory.getLogger(OnedayController.class);

	// list all 진행중
	@RequestMapping(value = "/allproceed", method = RequestMethod.GET)
	public ResponseEntity<List<OnedayVO>> listAllProceed() {
		ResponseEntity<List<OnedayVO>> entity = null;
		try {

			List<OnedayVO> list=service.listAllProceed();
			entity = new ResponseEntity<>(list, HttpStatus.OK);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			entity = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return entity;
	}

	// list all 진행예정
	@RequestMapping(value = "/allcoming", method = RequestMethod.GET)
	public ResponseEntity<List<OnedayVO>> listAllComing() {
		ResponseEntity<List<OnedayVO>> entity = null;
		try {

			entity = new ResponseEntity<>(service.listAllComing(), HttpStatus.OK);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			entity = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return entity;
	}

	// read one
	@Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.REPEATABLE_READ,rollbackFor = RuntimeException.class)
	@RequestMapping(value = "/{oneday_id}", method = RequestMethod.GET)
	public ResponseEntity<OnedayVO> read(@PathVariable("oneday") int oneday_id) {
		ResponseEntity<OnedayVO> entity = null;

		try {

			//d옵션이 있는거 요청하면 에러
			OnedayVO i=service.read(oneday_id);
			i.setOdyOptionVOList(service.readOptions(oneday_id));

			entity = new ResponseEntity<>(i, HttpStatus.OK);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			entity = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return entity;
	}

	//purchase
	@Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.REPEATABLE_READ,rollbackFor = RuntimeException.class)
	@RequestMapping(value = "/purchase/{ody_option_id}", method = RequestMethod.POST)
	public ResponseEntity<String> purchaseOneday(@PathVariable("ody_option_id") int ody_option_id,@RequestParam("access_token") String access_token) {
		ResponseEntity<String> entity = null;

		try {
			OdyPurchaseVO vo=new OdyPurchaseVO();
			vo.setOdy_option_id(ody_option_id);
			vo.setUser_id(jwtUtil.getUserID(access_token));
			//구매
			boolean flag=service.purchaseOneday(vo);
			if(flag){//구매 완료
				entity = new ResponseEntity<>("ok", HttpStatus.OK);
			}else{//중복구매
				entity = new ResponseEntity<>("duplicate", HttpStatus.OK);
			}


		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			entity = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return entity;
	}


}
