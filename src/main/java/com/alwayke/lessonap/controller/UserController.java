package com.alwayke.lessonap.controller;

import javax.inject.Inject;

import com.alwayke.lessonap.domain.ChangePasswordVO;
import com.alwayke.lessonap.domain.FindIDVO;
import com.alwayke.lessonap.service.AdminService;
import com.alwayke.lessonap.util.SMSUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.alwayke.lessonap.domain.UserVO;
import com.alwayke.lessonap.service.UserService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserService service;
	@Autowired
	private AdminService adminService;
	@Autowired
	private SMSUtil smsUtil;

	// 회원가입
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ResponseEntity<String> registUser(@RequestBody UserVO vo) {
		

		ResponseEntity<String> entity = null;
		try {

			service.registUser(vo);
			entity = new ResponseEntity<String>("ok", HttpStatus.OK);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			entity = new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}

		return entity;
	}

	// 아이디찾기
	@Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.REPEATABLE_READ,rollbackFor = RuntimeException.class)
	@RequestMapping(value = "/id", method = RequestMethod.POST)
	public ResponseEntity<String> findId(@RequestBody FindIDVO vo) {

		ResponseEntity<String> entity = null;
		try {

			String user_email=service.findId(vo);
			if(user_email==null){
				entity=new ResponseEntity<String>("fail",HttpStatus.OK);
			}else{
				smsUtil.sendEmail(vo.getPhone(),user_email);
				entity = new ResponseEntity<String>("ok", HttpStatus.OK);

			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			entity = new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}

		return entity;
	}
	// 패스워드찾기
	@Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.REPEATABLE_READ,rollbackFor = RuntimeException.class)
	@RequestMapping(value = "/password", method = RequestMethod.POST)
	public ResponseEntity<String> findPassword(@RequestBody FindIDVO vo) {

		ResponseEntity<String> entity = null;
		try {

			UserVO uvo=service.findPassword(vo);
			if(uvo==null){
				entity=new ResponseEntity<String>("fail",HttpStatus.OK);
			}else{
				ChangePasswordVO cpvo=new ChangePasswordVO(uvo.getUser_id(),uvo.getUser_birth());
				adminService.setPassword(cpvo);
				smsUtil.sendEmailPassword(vo.getPhone());
				entity = new ResponseEntity<String>("ok", HttpStatus.OK);

			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			entity = new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}

		return entity;
	}

}

