package com.alwayke.lessonap.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletContext;

import com.alwayke.lessonap.domain.LikeVO;
import com.alwayke.lessonap.domain.OptionVO;
import com.alwayke.lessonap.service.LikeService;
import com.alwayke.lessonap.util.JwtUtil;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.alwayke.lessonap.domain.ItemVO;
import com.alwayke.lessonap.domain.UserVO;
import com.alwayke.lessonap.service.ItemService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/items")
public class ItemController {

	@Autowired
	private ItemService service;
	@Autowired
	private LikeService likeService;
	@Autowired
	private JwtUtil jwtUtil;
	private static final Logger logger = LoggerFactory.getLogger(ItemController.class);

	// list all 진행중
	@RequestMapping(value = "/allproceed", method = RequestMethod.GET)
	public ResponseEntity<List<ItemVO>> listAllProceed() {
		ResponseEntity<List<ItemVO>> entity = null;
		try {

			List<ItemVO> list=service.listAllProceed();
			entity = new ResponseEntity<List<ItemVO>>(list, HttpStatus.OK);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			entity = new ResponseEntity<List<ItemVO>>(HttpStatus.BAD_REQUEST);
		}

		return entity;
	}

	// list all 진행예정
	@RequestMapping(value = "/allcoming", method = RequestMethod.GET)
	public ResponseEntity<List<ItemVO>> listAllComing() {
		ResponseEntity<List<ItemVO>> entity = null;
		try {

			entity = new ResponseEntity<>(service.listAllComing(), HttpStatus.OK);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			entity = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return entity;
	}

	// read one
	@Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.REPEATABLE_READ,rollbackFor = RuntimeException.class)
	@RequestMapping(value = "/{item_id}", method = RequestMethod.GET)
	public ResponseEntity<ItemVO> read(@PathVariable("item_id") int item_id,@RequestParam(value="access_token", required=false, defaultValue="0")String access_token){
		ResponseEntity<ItemVO> entity = null;
		int like_flag=0;
		try {

			if (!access_token.equals("0")){//로그인 했을시
				//좋아요 유무
				int user_id=jwtUtil.getUserID(access_token);
				if(likeService.checkLike(new LikeVO(item_id,user_id))==1){
					like_flag=1;
				}

			}
			ItemVO i=service.read(item_id);
			i.setItem_like_flag(like_flag);
			i.setOptionVOList(service.readOptions(item_id));

			entity = new ResponseEntity<ItemVO>(i, HttpStatus.OK);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			entity = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return entity;
	}

}
