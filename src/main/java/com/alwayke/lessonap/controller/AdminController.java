package com.alwayke.lessonap.controller;

import com.alwayke.lessonap.domain.*;
import com.alwayke.lessonap.service.AdminService;
import com.alwayke.lessonap.service.BidService;
import com.alwayke.lessonap.service.ItemService;
import com.alwayke.lessonap.util.Paging;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/admin")
public class AdminController {

	private static final Logger logger = LoggerFactory.getLogger(AdminController.class);

	@Autowired
	private AdminService adminService;

	//관리자 페이지 로그인
	@RequestMapping(value = "/login",method = RequestMethod.POST)
	public String doLogin(HttpServletRequest request,Model model) {
		String username=(String)request.getParameter("username");
		String password=(String)request.getParameter("password");

		HttpSession session=request.getSession();
		if(username.equals("admin")&&password.equals("1234")){//아이디&비번
			if(session.getAttribute("login")!=null) {
				session.removeAttribute("login");
			}else{
				session.setAttribute("login","admin");
			}
			return "home";
		}else{
			return "login";
		}
	}
	@RequestMapping(value = "/home",method = RequestMethod.GET)
	public String home(Model model) {
		return "home";
	}

	@RequestMapping(value = "/login",method = RequestMethod.GET)
	public String loginPage(Model model) {
			return "login";
	}


	//관리자 페이지 로그아웃
	@RequestMapping(value="/logout", method = RequestMethod.GET)
	public String logoutPage (HttpServletRequest request,Model model) {
		HttpSession session = request.getSession();
		if (session.getAttribute("login") != null) {
			session.removeAttribute("login");
		}
		return "login";
	}

    //유저 목록
    @RequestMapping(value = "/users",method = RequestMethod.GET)
	public String listAllUsers(Model model,@RequestParam(value = "page",required = false,defaultValue = "1")int page){
    	try {

			int start=40*(page-1);//불러올 페이지
			Paging paging = new Paging();
			paging.setPageNo(page);
			paging.setPageSize(40);
			paging.setTotalCount(adminService.countUsers());

			model.addAttribute("list",adminService.listAllUsers(start));
			model.addAttribute("paging",paging);

		}catch (Exception e){
    		e.printStackTrace();
		}
	return "user/list_users";
	}
	//유저 상세보기
	@RequestMapping(value = "/user/{user_id}",method = RequestMethod.GET)
	public String getUserById(Model model,@PathVariable("user_id")int user_id){
		try {

			model.addAttribute(adminService.getUserById(user_id));

		}catch (Exception e){
			e.printStackTrace();
		}
		return "user/read_user";
	}

	//유저 검색
	@RequestMapping(value = "/user",method = RequestMethod.GET)
	public String findUser(@RequestParam("name")String name, Model model){
		try {

			model.addAttribute("list",adminService.listUsersByName(name));
		}catch (Exception e){
			e.printStackTrace();
		}
		return "user/list_users";
	}

	//아티스트 설정
	@RequestMapping(value = "/user/artist/{user_id}",method = RequestMethod.GET)
	public String setAsArtist(@PathVariable("user_id")int user_id, Model model){
		try {
			adminService.setAsArtist(user_id);
			model.addAttribute("result","ok");
		}catch (Exception e){
			e.printStackTrace();
			model.addAttribute("result","error");
		}
		return "result";
	}
	//패스워드 유저의 생년월일로 변경
	@RequestMapping(value = "/user/password",method = RequestMethod.GET)
	public String setPassword(@RequestParam("user_id")int user_id,@RequestParam("user_birth")String user_birth, Model model){
		try {
			ChangePasswordVO vo=new ChangePasswordVO(user_id,user_birth);
			adminService.setPassword(vo);
			model.addAttribute("result","ok");
		}catch (Exception e){
			e.printStackTrace();
			model.addAttribute("result","error");
		}
		return "result";
	}

	@RequestMapping(value = "/inquiry",method = RequestMethod.GET)
	public String listAllInquiry(Model model){
		try {
			model.addAttribute("list",adminService.listAllInquiry());
		}catch (Exception e){
			e.printStackTrace();
		}
		return "inquiry/list_inquiry";
	}
	@RequestMapping(value = "/inquiry/done",method = RequestMethod.GET)
	public String listAllInquiryDone(Model model){
		try {
			model.addAttribute("list",adminService.listAllInquiryDone());
		}catch (Exception e){
			e.printStackTrace();
		}
		return "inquiry/list_inquiry";
	}
	//질문 상세 보기
	@RequestMapping(value = "/inquiry/{inquiry_id}",method = RequestMethod.GET)
	public String getInquiry(@PathVariable("inquiry_id")int inquiry_id, Model model){
		try {
			model.addAttribute("inquiry",adminService.getInquiry(inquiry_id));
		}catch (Exception e){
			e.printStackTrace();
		}
		return "inquiry/read_inquiry";
	}

	//질문 답
	@RequestMapping(value = "/inquiry/{inquiry_id}",method = RequestMethod.POST)
	public String answerInquiry(@PathVariable("inquiry_id")int inquiry_id,HttpServletRequest request){
		try {
			AnswerVO vo=new AnswerVO();
			vo.setInquiry_answer(request.getParameter("inquiry_answer"));
			vo.setInquiry_id(inquiry_id);
			adminService.answerInquiry(vo);
		}catch (Exception e){
			e.printStackTrace();
		}
		return "home";
	}

	//전체 문자 보내기
	@RequestMapping(value = "/sms",method = RequestMethod.POST)
	public String sendSmsAll(HttpServletRequest request){
		String message=request.getParameter("message");
		String returnPage="home";
		try {
			adminService.sendSMS(message);
		}catch (Exception e){
			e.printStackTrace();
			returnPage="error";
		}
		return returnPage;

	}
	// 문자 보내기
	@RequestMapping(value = "/sms",method = RequestMethod.GET)
	public String smsPage(){

		return "sms";

	}

}
