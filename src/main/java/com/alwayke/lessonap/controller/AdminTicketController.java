package com.alwayke.lessonap.controller;

import com.alwayke.lessonap.domain.*;
import com.alwayke.lessonap.service.TicketService;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.inject.Inject;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;

/**
 * Created by hs on 2017-05-29.
 */

@Controller
@RequestMapping("/admin/ticket")
public class AdminTicketController {

    @Inject
    private TicketService service;

    @RequestMapping(value = "",method = RequestMethod.GET)
    public String registTicket(Model model) {
        return "ticket/registTicket";
    }

    //진행중 원데이클래스 상세보기
    @Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.REPEATABLE_READ,rollbackFor = RuntimeException.class)
    @RequestMapping(value = "/read/proceed", method = RequestMethod.GET)
    public String readProceed(@RequestParam("ticket_id")int ticket_id, Model model) {

        try {
            TicketVO vo=service.read(ticket_id);
            List<TicketOptionVO> list=service.readOptions(ticket_id);
            vo.setTicketOptionVOList(list);

            for(int i=0;i<list.size();i++){
                int id=list.get(i).getTic_option_id();
            }

            model.addAttribute("ticketVO",vo);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "ticket/read_proceed";
    }

    @Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.REPEATABLE_READ,rollbackFor = RuntimeException.class)
    @RequestMapping(value="", method= RequestMethod.POST,produces = "application/json")
    public String insertTikcetFix(MultipartHttpServletRequest request){
        try {

            System.out.println("get value = "+request.getParameter("end_date"));
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");


            //아이템 등록
            int ticket_id=service.create(new TicketVO(df.parse(request.getParameter("end_date")),df.parse(request.getParameter("start_date")),Integer.parseInt(request.getParameter("price")),request.getParameter("content"),request.getParameter("title")));

            //이미지 업로드
//            String path=request.getSession().getServletContext().getRealPath("/")+"/images/";
			String path="C://images/";
            File dir=new File(path);
            System.out.println("path : "+path);
            if(!dir.isDirectory()){
                dir.mkdir();
            }
            Iterator<String> files=request.getFileNames();
            int counter=1;
            while (files.hasNext()){
                MultipartFile mFile = request.getFile(files.next());

                mFile.transferTo(new File(path+"ticket_"+ticket_id+"_"+counter+".png"));

                counter++;
            }

            if(ticket_id==0){
               throw new Exception();
            }

            //이미지 경로 업로드
            service.createImg(new ImageVO(ticket_id,"/fiximg/ticket_"+ticket_id+"_1.png","/fiximg/ticket_"+ticket_id+"_2.png"));

            //옵션 등록
            String[] option_name=request.getParameterValues("option_name");
            String[] max_num=request.getParameterValues("max_num");
            for (int i=0;i<option_name.length;i++){
                service.createOption(new TicketOptionVO(ticket_id,option_name[i],Integer.parseInt(max_num[i])));
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return "home";
    }

    @RequestMapping(value = "/proceed", method = RequestMethod.GET)
    public String proceed(Model model) {
        try {
            model.addAttribute("list", service.listAllProceed());
        }catch (Exception e){
            e.printStackTrace();
        }


        return "ticket/list_proceed";
    }
    @RequestMapping(value = "/soon", method = RequestMethod.GET)
    public String soon(Model model) {
        try {
            model.addAttribute("list", service.listAllComing());
        }catch (Exception e){
            e.printStackTrace();
        }

        return "ticket/list_soon";
    }

    @RequestMapping(value = "/end", method = RequestMethod.GET)
    public String end(Model model) {
        try {
            model.addAttribute("list", service.listAllEnd());
        }catch (Exception e){
            e.printStackTrace();
        }

        return "ticket/list_end";
    }
    //구매 명단 보기
    @RequestMapping(value = "/option/{tic_option_id}", method = RequestMethod.GET)
    public String listAllPurchase(@PathVariable("tic_option_id")int tic_option_id, Model model) {

        try {
            List<TicPurchaseDTO> list=service.listAllPurchase(tic_option_id);

            model.addAttribute("list",list);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "table_bid";
    }
}
