package com.alwayke.lessonap.controller;

import com.alwayke.lessonap.domain.NoticeVO;
import com.alwayke.lessonap.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by hs on 2017-05-29.
 */

@RestController
@RequestMapping("/notice")
public class NoticeController {

    @Autowired
    private NoticeService service;

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public ResponseEntity<List<NoticeVO>> listNotice(){
        ResponseEntity<List<NoticeVO>> entity=null;
        try {
            entity=new ResponseEntity<>(service.listNotice(), HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            entity=new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return entity;
    }

    @RequestMapping(value = "/{notice_id}",method = RequestMethod.GET)
    public ResponseEntity<NoticeVO> readNotice(@PathVariable("notice_id")int notice_id){
        ResponseEntity<NoticeVO> entity=null;
        try {
            entity=new ResponseEntity<>(service.readNotice(notice_id), HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            entity = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return entity;
    }

}
