package com.alwayke.lessonap.controller;

import com.alwayke.lessonap.domain.*;
import com.alwayke.lessonap.service.AdminService;
import com.alwayke.lessonap.service.BidService;
import com.alwayke.lessonap.service.ItemService;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Controller
@RequestMapping("/admin/hotdeal")
public class AdminHotdealController {

	private static final Logger logger = LoggerFactory.getLogger(AdminHotdealController.class);

	@Inject
	private ItemService service;
	@Inject
	private BidService bidService;
	@Inject
	private AdminService adminService;

	//진행중 핫딜 상세보기
	@Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.REPEATABLE_READ,rollbackFor = RuntimeException.class)
	@RequestMapping(value = "/read/proceed", method = RequestMethod.GET)
	public String readProceed(@RequestParam("item_id")int item_id, Model model) {

		try {
			ItemVO vo=service.read(item_id);
			List<OptionVO> list=service.readOptions(item_id);
			vo.setOptionVOList(list);

			for(int i=0;i<list.size();i++){
				int id=list.get(i).getOption_id();
				list.get(i).setBid_count(bidService.countBidOnOption(id));
			}

			model.addAttribute(vo);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "hotdeal/read_proceed";
	}
	//예정 핫딜 상세보기
	@Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.REPEATABLE_READ,rollbackFor = RuntimeException.class)
	@RequestMapping(value = "/read/soon", method = RequestMethod.GET)
	public String readSoon(@RequestParam("item_id")int item_id, Model model) {

		try {
			ItemVO vo=service.read(item_id);
			List<OptionVO> list=service.readOptions(item_id);
			vo.setOptionVOList(list);

			model.addAttribute(vo);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "hotdeal/read_soon";
	}
	//결제 대기중 핫딜 상세보기
	@Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.REPEATABLE_READ,rollbackFor = RuntimeException.class)
    @RequestMapping(value = "/read/ready", method = RequestMethod.GET)
    public String readReady(@RequestParam("item_id")int item_id, Model model) {

        try {
            ItemVO vo=service.read(item_id);
            List<OptionVO> list=service.readOptions(item_id);
            vo.setOptionVOList(list);

            model.addAttribute(vo);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "hotdeal/read_ready";
    }
	//종료 핫딜 상세보기
	@Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.REPEATABLE_READ,rollbackFor = RuntimeException.class)
	@RequestMapping(value = "/read/end", method = RequestMethod.GET)
	public String readEnd(@RequestParam("item_id")int item_id, Model model) {

		try {
			ItemVO vo=service.read(item_id);
			List<OptionVO> list=service.readOptions(item_id);
			vo.setOptionVOList(list);

			model.addAttribute(vo);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "hotdeal/read_end";
	}

	//입찰 종료 핫딜 상세보기
	@Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.REPEATABLE_READ,rollbackFor = RuntimeException.class)
	@RequestMapping(value = "/read/bidend", method = RequestMethod.GET)
	public String readBidEnd(@RequestParam("item_id")int item_id, Model model) {

		try {
			ItemVO vo=service.read(item_id);
			List<OptionVO> list=service.readOptions(item_id);
			vo.setOptionVOList(list);

			for(int i=0;i<list.size();i++){
				int id=list.get(i).getOption_id();
				list.get(i).setBid_count(bidService.countBidOnOption(id));
			}

			model.addAttribute(vo);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "hotdeal/read_bidend";
	}


	@RequestMapping(value = "/option/{option_id}", method = RequestMethod.GET)
	public String listAllBidOnOption(@PathVariable("option_id")int option_id, Model model) {

		try {
			List<BidVO> list=bidService.listAllBidOnOption(option_id);

			model.addAttribute("list",list);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "table_bid";
	}
	@RequestMapping(value = "/done", method = RequestMethod.POST)
	public String determination(HttpServletRequest request, Model model) {

		try {
			//update item flag

			//낙찰가 입력
			int item_id=Integer.parseInt(request.getParameter("item_id"));
	String[] price=request.getParameterValues("option_price");
		String[] option_id=request.getParameterValues("option_id");

		service.finishAuction(item_id,price,option_id);

			JSONObject object=new JSONObject();
		object.put("result","ok");
		model.addAttribute("result",object);
		}catch (Exception e){
			e.printStackTrace();
		}

		return "result";
	}

	@Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.REPEATABLE_READ,rollbackFor = RuntimeException.class)
	@RequestMapping(value="", method=RequestMethod.POST,produces = "application/json")
	public String insertHotdeal(MultipartHttpServletRequest request){
		String returnPage="home";
		try {

			System.out.println("get value = "+request.getParameter("end_date"));
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");


			//아이템 등록
			int item_id=service.create(new ItemVO(df.parse(request.getParameter("end_date")),df.parse(request.getParameter("start_date")),Integer.parseInt(request.getParameter("max_price")),Integer.parseInt(request.getParameter("min_price")),request.getParameter("content"),request.getParameter("title"),request.getParameter("link")));

			//이미지 업로드
//			String path=request.getSession().getServletContext().getRealPath("/")+"/images/";
			String path="C://images/";
			File dir=new File(path);
			System.out.println("path : "+path);
			if(!dir.isDirectory()){
				dir.mkdir();
			}
			Iterator<String> files=request.getFileNames();
			int counter=1;
			ArrayList<String> filelist=new ArrayList<>();
			while (files.hasNext()){
				MultipartFile mFile = request.getFile(files.next());
				String fileName[]=mFile.getOriginalFilename().split("\\.");
				filelist.add(fileName[fileName.length-1]);
				mFile.transferTo(new File(path+item_id+"_"+counter+"."+fileName[fileName.length-1]));

				counter++;
			}

			//이미지 경로 업로드
			service.createImg(new ImageVO(item_id,"/fiximg/"+item_id+"_1."+filelist.get(0),"/fiximg/"+item_id+"_2."+filelist.get(1),"/fiximg/"+item_id+"_3."+filelist.get(2)));

			//옵션 등록
			String[] option_name=request.getParameterValues("option_name");
			String[] min_num=request.getParameterValues("min_num");

			for (int i=0;i<option_name.length;i++){
				service.createOption(new OptionVO(item_id,option_name[i],Integer.parseInt(min_num[i])));
			}
		}catch (Exception e){
			e.printStackTrace();

			returnPage="error";
		}
		return returnPage;
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String hotdeal(Model model) {

		return "hotdeal/hotdeal";
	}

	@RequestMapping(value = "/proceed", method = RequestMethod.GET)
	public String proceed(Model model) {
		try {

		model.addAttribute("list", service.listAllProceed());

		}catch (Exception e){
			e.printStackTrace();
		}


		return "hotdeal/list_proceed";
	}
	@RequestMapping(value = "/soon", method = RequestMethod.GET)
	public String soon(Model model) {
		try {

			model.addAttribute("list", service.listAllComing());

		}catch (Exception e){
			e.printStackTrace();
		}


		return "hotdeal/list_soon";
	}
	@RequestMapping(value = "/bidend", method = RequestMethod.GET)
	public String bidEnd(Model model) {
		try {

			model.addAttribute("list", service.listAllBidEnd());

		}catch (Exception e){
			e.printStackTrace();
		}


		return "hotdeal/list_bid_end";
	}
	@RequestMapping(value = "/end", method = RequestMethod.GET)
	public String end(Model model) {
		try {

			model.addAttribute("list", service.listAllBidEnd());

		}catch (Exception e){
			e.printStackTrace();
		}


		return "hotdeal/list_end";
	}

	//결제 대기 핫딜
	@RequestMapping(value = "/ready", method = RequestMethod.GET)
	public String ready(Model model) {
		try {

			model.addAttribute("list", service.listAllReadyPayment());

		}catch (Exception e){
			e.printStackTrace();
		}


		return "hotdeal/list_ready";
	}

	//낙찰 입금 목록 불러오기
	@RequestMapping(value = "/ready/payment/{option_id}",method = RequestMethod.GET)
    public String listOptionPayment(@PathVariable("option_id") int option_id, Model model){
        try {

            model.addAttribute("list",adminService.listOptionPayment(option_id));

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

        return "table_bid_pay";
    }

	//유저 이름으로 입금된 입금 목록 불러오기
	@RequestMapping(value = "/payment",method = RequestMethod.GET)
	public String listPaymentByUsername(@RequestParam("user_name") String user_name, Model model){
		try {

			model.addAttribute("list",adminService.listPaymentByUsername(user_name));

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return "table_payment";
	}

	//유저 이름으로 입금된 입금 입금 확인하기
	@ResponseBody
	@RequestMapping(value = "/payment",method = RequestMethod.POST)
	public String paymentDone(@RequestBody PaymentVO vo){
		JSONObject object=new JSONObject();
		try {
			adminService.paymentDone(vo);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return "ok";
	}

	//조기종료
	@RequestMapping(value = "/finish/{item_id}",method = RequestMethod.GET)
	public String paymentDone(@PathVariable("item_id")int item_id){
		try {
			service.forceFinishAuction(item_id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return "home";
	}


}
