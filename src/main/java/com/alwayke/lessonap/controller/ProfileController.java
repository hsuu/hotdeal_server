package com.alwayke.lessonap.controller;

import javax.inject.Inject;

import com.alwayke.lessonap.domain.AccountVO;
import com.alwayke.lessonap.domain.InquiryVO;
import com.alwayke.lessonap.domain.PhoneVO;
import com.alwayke.lessonap.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.alwayke.lessonap.domain.UserVO;
import com.alwayke.lessonap.service.UserService;
import com.alwayke.lessonap.service.UserServiceImpl;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/profile")
public class ProfileController {

	@Autowired
	private UserService service;
	@Autowired
	private JwtUtil jwtUtil;

	// 프로필 페이지
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ResponseEntity<UserVO> getProfile(@RequestParam("access_token") String access_token) {
		ResponseEntity<UserVO> entity = null;
		int user_id=0;
		try {

			user_id=jwtUtil.getUserID(access_token);

		}catch (Exception e){
			e.printStackTrace();
		}

		try {

			entity = new ResponseEntity<UserVO>(service.getUser(user_id), HttpStatus.OK);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			entity = new ResponseEntity<UserVO>(HttpStatus.BAD_REQUEST);
		}

		return entity;
	}

	// 프로필 변경
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ResponseEntity<String> updateProfile(@RequestBody UserVO vo) {
		ResponseEntity<String> entity = null;
		try {

			entity = new ResponseEntity<String>("a", HttpStatus.OK);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			entity = new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}

		return entity;
	}

	//핸드폰 번호 변경
	@RequestMapping(value = "/phone", method = RequestMethod.POST)
	public ResponseEntity<String> updatePhone(@RequestBody PhoneVO vo,@RequestParam("access_token") String access_token) {
		ResponseEntity<String> entity = null;
		int user_id=0;
		try {
			user_id=jwtUtil.getUserID(access_token);
			vo.setUser_id(user_id);
			service.updatePhone(vo);
			entity = new ResponseEntity<String>("ok", HttpStatus.OK);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			entity = new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}

		return entity;
	}


	// 계좌 정보 페이지
	@RequestMapping(value = "/account", method = RequestMethod.GET)
	public ResponseEntity<AccountVO> getUserAccount(@RequestParam("access_token") String access_token) {
		ResponseEntity<AccountVO> entity = null;
		int user_id=0;
		try {
			user_id=jwtUtil.getUserID(access_token);
			entity = new ResponseEntity<>(service.getUserAccount(user_id), HttpStatus.OK);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			entity = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return entity;
	}

	// 결제정보 변경
	@RequestMapping(value = "/account", method = RequestMethod.POST)
	public ResponseEntity<String> updateAccount(@RequestBody AccountVO vo,@RequestParam("access_token") String access_token) {
		ResponseEntity<String> entity = null;
		int user_id=0;
		try {
			user_id=jwtUtil.getUserID(access_token);
			vo.setUser_id(user_id);
			service.updateAccount(vo);
			entity = new ResponseEntity<>("ok", HttpStatus.OK);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			entity = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return entity;
	}


	// 문의하기
	@RequestMapping(value = "/inquiry", method = RequestMethod.POST)
	public ResponseEntity<String> inquiry(@RequestBody InquiryVO vo, @RequestParam("access_token") String access_token) {
		ResponseEntity<String> entity = null;
		int user_id=0;
		try {
			user_id=jwtUtil.getUserID(access_token);
			vo.setUser_id(user_id);
			service.inquiry(vo);
			entity = new ResponseEntity<String>("ok", HttpStatus.OK);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			entity = new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}

		return entity;
	}
	// 대기중 문의 보기
	@RequestMapping(value = "/inquiry", method = RequestMethod.GET)
	public ResponseEntity<List<InquiryVO>> inquiryAllWaiting(@RequestParam("access_token") String access_token) {
		ResponseEntity<List<InquiryVO>> entity = null;
		int user_id=0;
		try {
			user_id=jwtUtil.getUserID(access_token);
			entity = new ResponseEntity<List<InquiryVO>>(service.listInquiryWaiting(user_id), HttpStatus.OK);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			entity = new ResponseEntity<List<InquiryVO>>(HttpStatus.BAD_REQUEST);
		}

		return entity;
	}
	// 완료 문의 보기
	@RequestMapping(value = "/inquiry/end", method = RequestMethod.GET)
	public ResponseEntity<List<InquiryVO>> inquiryAllEnd(@RequestParam("access_token") String access_token) {
		ResponseEntity<List<InquiryVO>> entity = null;
		int user_id=0;
		try {
			user_id=jwtUtil.getUserID(access_token);
			entity = new ResponseEntity<List<InquiryVO>>(service.listInquiryEnd(user_id), HttpStatus.OK);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			entity = new ResponseEntity<List<InquiryVO>>(HttpStatus.BAD_REQUEST);
		}

		return entity;
	}

}
