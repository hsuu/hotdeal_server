package com.alwayke.lessonap.controller;

import javax.inject.Inject;

import com.alwayke.lessonap.domain.BidDTO;
import com.alwayke.lessonap.domain.BidVO;
import com.alwayke.lessonap.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.alwayke.lessonap.service.BidService;

import java.util.List;

@RestController
@RequestMapping("/bid")
public class BidController {

	@Autowired
	private BidService service;

	@Autowired
	private JwtUtil jwtUtil;
	// 입찰
	@RequestMapping(value = "/", method = RequestMethod.POST)
	@Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.REPEATABLE_READ,rollbackFor = RuntimeException.class)
	public ResponseEntity<String> bidding(@RequestBody BidDTO dto,@RequestParam("access_token") String access_token) {
		ResponseEntity<String> entity = null;
		//option_id,bid_price

		try {
			dto.setUser_id(jwtUtil.getUserID(access_token));

			//이미 구매한 상품인지 체크
			if(service.checkBidding(dto)==0) {
				service.bidding(dto);
				entity = new ResponseEntity<String>("ok", HttpStatus.OK);
			}else{
				//이미 구매한 상품일 경우
				entity = new ResponseEntity<String>("duplicate", HttpStatus.NOT_ACCEPTABLE);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			entity = new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}

		return entity;
	}

	// 입찰취소
	@RequestMapping(value = "/{option_id}", method = RequestMethod.PUT)
	public ResponseEntity<String> biddingCancel(@PathVariable("option_id") int option_id) {
		ResponseEntity<String> entity = null;
		try {
			service.biddingCancel(option_id);
			entity = new ResponseEntity<String>("ok", HttpStatus.OK);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			entity = new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}

		return entity;
	}
	
	//list all 입찰
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ResponseEntity<List<BidVO>> listAllBid(@RequestParam("access_token") String access_token) {
		ResponseEntity<List<BidVO>> entity = null;
		//token에서  user_id 가져온다음 넣어줌

		try {

			entity = new ResponseEntity<List<BidVO>>(service.listAllbid(jwtUtil.getUserID(access_token)), HttpStatus.OK);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			entity = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return entity;
	}
	//list all cancel
	@RequestMapping(value = "/cancel", method = RequestMethod.GET)
	public ResponseEntity<List<BidVO>> listAllCancelBid(@RequestParam("access_token") String access_token) {
		ResponseEntity<List<BidVO>> entity = null;
		try {

			entity = new ResponseEntity<List<BidVO>>(service.listAllCancelbid(jwtUtil.getUserID(access_token)), HttpStatus.OK);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			entity = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return entity;
	}


}
