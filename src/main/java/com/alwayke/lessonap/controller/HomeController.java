package com.alwayke.lessonap.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by hs on 2017-06-12.
 */
@Controller
@RequestMapping("")
public class HomeController {
    // 구글설문지 이동
    @RequestMapping(value = "/jehu", method = RequestMethod.GET)
    public String googleRedirect(){
        return "jehu";
    }
    // 구글설문지2 이동
    @RequestMapping(value = "/jehusecond", method = RequestMethod.GET)
    public String googleRedirect2(){
        return "jehusecond";
    }

}
