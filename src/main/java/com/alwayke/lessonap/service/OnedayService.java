package com.alwayke.lessonap.service;

import com.alwayke.lessonap.domain.*;

import java.util.List;

public interface OnedayService {

  public OnedayVO read(int oneday_id) throws Exception;

  public List<OdyOptionVO> readOptions(int oneday_id) throws Exception;

  public int create(OnedayVO vo) throws Exception;
  public void createImg(ImageVO vo) throws Exception;
  public void createOption(OdyOptionVO vo)throws Exception;

  public List<OnedayVO> listAllProceed() throws Exception;
  public List<OnedayVO> listAllComing() throws Exception;
  public List<OnedayVO> listAllEnd() throws Exception;

  public boolean purchaseOneday(OdyPurchaseVO vo)throws Exception;
  public List<OdyPurchaseDTO> listAllPurchase(int ody_option_id) throws Exception;
  public void updatePurchase(int ody_purchase_id)throws Exception;


}
