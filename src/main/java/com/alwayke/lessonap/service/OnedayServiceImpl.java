package com.alwayke.lessonap.service;

import com.alwayke.lessonap.domain.*;
import com.alwayke.lessonap.persistence.OnedayDAO;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

@Service
public class OnedayServiceImpl implements OnedayService{

	@Inject
	private OnedayDAO dao;

	@Override
	public OnedayVO read(int oneday_id) throws Exception {
		return dao.read(oneday_id);
	}

	@Override
	public List<OdyOptionVO> readOptions(int oneday_id) throws Exception {
		return dao.readOptions(oneday_id);
	}

	@Override
	public int create(OnedayVO vo) throws Exception {
		return dao.create(vo);
	}

	@Override
	public void createImg(ImageVO vo) throws Exception {
		dao.createImg(vo);
	}

	@Override
	public void createOption(OdyOptionVO vo) throws Exception {
		dao.createOption(vo);
	}

	@Override
	public List<OnedayVO> listAllProceed() throws Exception {
		return dao.listAllProceed();
	}

	@Override
	public List<OnedayVO> listAllComing() throws Exception {
		return dao.listAllComing();
	}

	@Override
	public List<OnedayVO> listAllEnd() throws Exception {
		return dao.listAllEnd();
	}

	@Override
	public boolean purchaseOneday(OdyPurchaseVO vo) throws Exception {
		int flag=dao.checkPurchase(vo.getOdy_option_id());
		if(flag>0){
			return false;
		}else{
			dao.purchaseOneday(vo);
			return true;
		}
	}

	@Override
	public List<OdyPurchaseDTO> listAllPurchase(int ody_option_id) throws Exception {
		return dao.listAllPurchase(ody_option_id);
	}

	@Override
	public void updatePurchase(int ody_purchase_id) throws Exception {
		dao.updatePurchaseFlag(ody_purchase_id);
	}
}
