package com.alwayke.lessonap.service;

import com.alwayke.lessonap.domain.*;
import com.alwayke.lessonap.persistence.OnedayDAO;
import com.alwayke.lessonap.persistence.TicketDAO;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

@Service
public class TicketServiceImpl implements TicketService{

	@Inject
	private TicketDAO dao;

	@Override
	public TicketVO read(int ticket_id) throws Exception {
		return dao.read(ticket_id);
	}

	@Override
	public List<TicketOptionVO> readOptions(int ticket_id) throws Exception {
		return dao.readOptions(ticket_id);
	}

	@Override
	public int create(TicketVO vo) throws Exception {
		return dao.create(vo);
	}

	@Override
	public void createImg(ImageVO vo) throws Exception {
		dao.createImg(vo);
	}

	@Override
	public void createOption(TicketOptionVO vo) throws Exception {
		dao.createOption(vo);
	}

	@Override
	public List<TicketVO> listAllProceed() throws Exception {
		return dao.listAllProceed();
	}

	@Override
	public List<TicketVO> listAllComing() throws Exception {
		return dao.listAllComing();
	}

	@Override
	public List<TicketVO> listAllEnd() throws Exception {
		return dao.listAllEnd();
	}

	@Override
	public boolean purchaseTicket(TicPurchaseVO vo) throws Exception {
		int flag=dao.checkPurchase(vo.getTic_option_id());
		if(flag>0){
			return false;
		}else{
			dao.purchaseTicket(vo);
			return true;
		}
	}

	@Override
	public List<TicPurchaseDTO> listAllPurchase(int tic_option_id) throws Exception {
		return dao.listAllPurchase(tic_option_id);
	}

	@Override
	public void updatePurchase(int tic_purchase_id) throws Exception {
		dao.updatePurchaseFlag(tic_purchase_id);
	}
}
