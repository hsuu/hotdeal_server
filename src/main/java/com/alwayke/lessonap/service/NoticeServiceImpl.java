package com.alwayke.lessonap.service;

import com.alwayke.lessonap.domain.NewsVO;
import com.alwayke.lessonap.domain.NoticeVO;
import com.alwayke.lessonap.persistence.NoticeDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

@Service
public class NoticeServiceImpl implements NoticeService{

 @Autowired
 private NoticeDAO dao;


 @Override
 public void insertNotice(NoticeVO vo) throws Exception {
  dao.insertNotice(vo);
 }

 @Override
 public List<NoticeVO> listNotice() throws Exception {
  return dao.listNotice();
 }

 @Override
 public NoticeVO readNotice(int notice_id) throws Exception {
  return dao.readNotice(notice_id);
 }

 @Override
 public void updateNotice(NoticeVO vo) throws Exception {
dao.updateNotice(vo);
 }

 @Override
 public void deleteNotice(int notice_id) throws Exception {
dao.deleteNotice(notice_id);
 }
}
