package com.alwayke.lessonap.service;

import java.util.List;

import com.alwayke.lessonap.domain.*;

public interface ItemService {

  public ItemVO read(int item_id) throws Exception;

  public List<OptionVO> readOptions(int item_id) throws Exception;

  public int create(ItemVO vo) throws Exception;

  public void createImg(ImageVO vo) throws Exception;

  public void createOption(OptionVO vo)throws Exception;

  public List<ItemVO> listAllProceed() throws Exception;

  public List<ItemVO> listAllComing() throws Exception;
  public List<ItemVO> listAllBidEnd() throws Exception;
  public List<ItemVO> listAllReadyPayment() throws Exception;
  public void finishAuction(int item_id,String[] price,String[] option_id) throws Exception;
  public void forceFinishAuction(int item_id) throws Exception;

}
