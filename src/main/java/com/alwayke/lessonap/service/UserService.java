package com.alwayke.lessonap.service;

import com.alwayke.lessonap.domain.*;

import java.util.List;

public interface UserService {

  public UserVO getUser(int user_id) throws Exception;
  
  public void registUser(UserVO vo) throws Exception;

  public void updateProfile(UserVO vo) throws Exception;

  public void inquiry(InquiryVO vo)throws Exception;
  public List<InquiryVO> listInquiryWaiting(int user_id) throws Exception;
  public List<InquiryVO> listInquiryEnd(int user_id) throws Exception;

  public void updatePhone(PhoneVO vo)throws Exception;

  public void updateAccount(AccountVO vo)throws Exception;
  public AccountVO getUserAccount(int user_id)throws Exception;

  public String findId(FindIDVO vo)throws Exception;
  public UserVO findPassword(FindIDVO vo)throws Exception;

}
