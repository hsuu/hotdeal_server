package com.alwayke.lessonap.service;

import com.alwayke.lessonap.domain.LikeVO;
import com.alwayke.lessonap.persistence.LikeDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by hs on 2017-06-28.
 */
@Service
public class LikeServiceImpl implements LikeService {
    @Autowired
    private LikeDAO dao;


    @Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.REPEATABLE_READ,rollbackFor = RuntimeException.class)
    @Override
    public int likeItem(LikeVO vo) throws Exception {

        int flag=dao.checkLike(vo);
        if(flag==1){ //좋아요 있을 경우
            dao.deleteLike(vo);
            dao.decreaseLike(vo.getItem_id());
        }else{ //좋아요 없을 경우
            dao.insertLike(vo);
            dao.increaseLike(vo.getItem_id());
        }
        return flag;
    }

    @Override
    public int checkLike(LikeVO vo) throws Exception {
        return dao.checkLike(vo);
    }
}
