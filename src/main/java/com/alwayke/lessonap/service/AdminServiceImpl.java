package com.alwayke.lessonap.service;

import com.alwayke.lessonap.domain.*;
import com.alwayke.lessonap.persistence.AdminDAO;
import com.alwayke.lessonap.util.SMSUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
@Service
public class AdminServiceImpl implements AdminService{

 @Autowired
 private AdminDAO dao;
@Autowired
private SMSUtil smsUtil;


 @Override
 public List<UserVO> listAllUsers(int start) throws Exception {
  return dao.listAllUsers(start);
 }

 @Override
 public UserVO getUserById(int user_id) throws Exception {
  return dao.getUserById(user_id);
 }

 @Override
 public int countUsers() throws Exception {
  return dao.countUsers();
 }

 @Override
 public List<UserVO> listUsersByName(String name) throws Exception {
  return dao.listUsersByName(name);
 }

 @Override
 public void setAsArtist(int user_id) throws Exception {
  dao.setAsArtist(user_id);
 }

 @Override
 public void setPassword(ChangePasswordVO vo) throws Exception {
  String birth=vo.getUser_birth();
  birth=birth.replaceAll("-","");

  //비번 암호화
  StringBuffer sbuf = new StringBuffer();
  MessageDigest mDigest = MessageDigest.getInstance("SHA-256");
  mDigest.update(birth.getBytes());

  byte[] msgStr = mDigest.digest() ;

  for(int i=0; i < msgStr.length; i++){
   byte tmpStrByte = msgStr[i];
   String tmpEncTxt = Integer.toString((tmpStrByte & 0xff) + 0x100, 16).substring(1);

   sbuf.append(tmpEncTxt) ;
  }
  vo.setUser_password(sbuf.toString());
  dao.setPassword(vo);
 }

 @Override
 public List<InquiryVO> listAllInquiry() throws Exception {
  return dao.listAllInquiry();
 }

 @Override
 public List<InquiryVO> listAllInquiryDone() throws Exception {
  return dao.listAllInquiryDone();
 }

 @Override
 public AnswerVO getInquiry(int inquiry_id) throws Exception {
  return dao.getInquiry(inquiry_id);
 }

 @Transactional
 @Override
 public void answerInquiry(AnswerVO vo) throws Exception {
   dao.answerInquiry(vo.getInquiry_id());
   dao.insertAnswer(vo);
 }

 @Override
 @Transactional
 public String deposit(DepositVO vo) {
  String result="fail";
  try {
   if(dao.checkPaymentTid(vo.getTid())>0){

   }else{
    dao.insertPayment(vo);
    result="OK";
   }

  }catch (Exception e){
   e.printStackTrace();
  }

  return result;
 }

 @Override
 public List<PaymentVO> listPaymentByUsername(String user_name) throws Exception {
  return dao.listPaymentByUsername(user_name);
 }

 @Override
 public void paymentDone(PaymentVO vo) throws Exception {
  dao.paymentDone(vo);
 }
 @Override
 public List<PaymentVO> listOptionPayment(int option_id) throws Exception {
  return dao.listOptionPayment(option_id);
 }

 @Override
 public void sendSMS(String message) throws Exception {
   List<String> list=dao.listSMSandMarketing();
   smsUtil.sendSMS((ArrayList<String>)list,message);
 }
}
