package com.alwayke.lessonap.service;

import com.alwayke.lessonap.domain.*;
import com.alwayke.lessonap.persistence.NewsDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NewsServiceImpl implements NewsService{

 @Autowired
 private NewsDAO dao;



 @Override
 public int insertNews(NewsVO vo) throws Exception {
  return dao.insertNews(vo);
 }

 @Override
 public List<NewsVO> listNews() throws Exception {
  return dao.listNews();
 }

 @Override
 public NewsVO readNews(int news_id) throws Exception {
  return dao.readNews(news_id);
 }

 @Override
 public void updateNews(NewsVO vo) throws Exception {
dao.updateNews(vo);
 }

 @Override
 public void deleteNews(int news_id) throws Exception {
dao.deleteNews(news_id);
 }
}
