package com.alwayke.lessonap.service;

import com.alwayke.lessonap.domain.LikeVO;

/**
 * Created by hs on 2017-06-28.
 */
public interface LikeService {

    public int likeItem(LikeVO vo) throws Exception;

    public int checkLike(LikeVO vo)throws Exception;
}
