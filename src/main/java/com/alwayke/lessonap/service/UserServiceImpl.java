package com.alwayke.lessonap.service;

import com.alwayke.lessonap.domain.*;
import com.alwayke.lessonap.persistence.UserDAO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

	  @Inject
	  private UserDAO dao;

	@Override
	public UserVO getUser(int user_id) throws Exception {
		// TODO Auto-generated method stub
		return dao.getUser(user_id);
	}

	@Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.REPEATABLE_READ,rollbackFor = RuntimeException.class)
	@Override
	public void registUser(UserVO vo) throws Exception {
		// TODO Auto-generated method stub
		dao.regist(vo);
		dao.insertAccount(vo.getUser_id());
	}

	@Override
	public void updateProfile(UserVO vo) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void inquiry(InquiryVO vo) throws Exception {
		dao.inquiry(vo);
	}

	@Override
	public List<InquiryVO> listInquiryWaiting(int user_id) throws Exception {
		return dao.listInquiryWaiting(user_id);
	}

	@Override
	public List<InquiryVO> listInquiryEnd(int user_id) throws Exception {
		return dao.listInquiryEnd(user_id);
	}

	@Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.REPEATABLE_READ,rollbackFor = RuntimeException.class)
	@Override
	public void updatePhone(PhoneVO vo) throws Exception {
		dao.insertPhoneLog(vo);
		dao.updatePhone(vo);
	}

	@Override
	public void updateAccount(AccountVO vo) throws Exception {
		dao.updateAccount(vo);
	}

	@Override
	public AccountVO getUserAccount(int user_id) throws Exception {
		return dao.getUserAccount(user_id);
	}

	@Override
	public String findId(FindIDVO vo) throws Exception {
		return dao.findId(vo);
	}

	@Override
	public UserVO findPassword(FindIDVO vo) throws Exception {
		return dao.findPassword(vo);
	}
}
