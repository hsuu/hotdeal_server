package com.alwayke.lessonap.service;

import com.alwayke.lessonap.domain.NewsVO;
import com.alwayke.lessonap.domain.NoticeVO;

import java.util.List;

public interface NoticeService {

 public void insertNotice(NoticeVO vo)throws Exception;
 public List<NoticeVO> listNotice()throws Exception;
 public NoticeVO readNotice(int notice_id)throws Exception;
 public void updateNotice(NoticeVO vo)throws Exception;
 public void deleteNotice(int notice_id)throws Exception;

}
