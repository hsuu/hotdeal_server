package com.alwayke.lessonap.service;

import java.util.List;

import javax.inject.Inject;

import com.alwayke.lessonap.domain.BidDTO;
import org.springframework.stereotype.Service;

import com.alwayke.lessonap.domain.BidVO;
import com.alwayke.lessonap.domain.ItemVO;
import com.alwayke.lessonap.persistence.BidDAO;
import com.alwayke.lessonap.persistence.ItemDAO;

@Service
public class BidServiceImpl implements BidService{

	  @Inject
	  private BidDAO dao;

	@Override
	public void bidding(BidDTO dto) throws Exception {
		System.out.println("******\n"+dto.getOption_id()+"\n"+dto.getUser_id()+"\n"+dto.getBid_price()+"\n*******");
		dao.bidding(dto);
	}

	@Override
	public void biddingCancel(int bid_id) throws Exception {
		dao.biddingCancel(bid_id);
	}

	@Override
	public List<BidVO> listAllbid(int user_id) throws Exception {
		// TODO Auto-generated method stub
		return dao.listAllBidding(user_id);
	}
	@Override
	public List<BidVO> listAllCancelbid(int user_id) throws Exception {
		return dao.listAllCancelBid(user_id);
	}

	@Override
	public List<BidVO> listAllBidOnItem(int user_id, int item_id) throws Exception {
		return null;
	}

	@Override
	public int checkBidding(BidDTO dto) throws Exception {
		return dao.checkBidding(dto);
	}

	@Override
	public List<BidVO> listAllBidOnOption(int option_id) throws Exception {
		return dao.listAllBidOnOption(option_id);
	}

	@Override
	public int countBidOnOption(int option_id) throws Exception {
		return dao.countBidOnOption(option_id);
	}
}
