package com.alwayke.lessonap.service;

import java.util.List;

import javax.inject.Inject;

import com.alwayke.lessonap.domain.*;
import org.springframework.stereotype.Service;

import com.alwayke.lessonap.persistence.ItemDAO;
import com.alwayke.lessonap.persistence.UserDAO;

@Service
public class ItemServiceImpl implements ItemService{

	  @Inject
	  private ItemDAO dao;

	@Override
	public ItemVO read(int item_id) throws Exception {
		// TODO Auto-generated method stub
		return dao.read(item_id);
	}

	@Override
	public List<OptionVO> readOptions(int item_id) throws Exception {
		return dao.readOptions(item_id);
	}

	@Override
	public int create(ItemVO vo) throws Exception {
		// TODO Auto-generated method stub
		return dao.create(vo);
	}

	@Override
	public void createImg(ImageVO vo) throws Exception {
		dao.createImg(vo);
	}

	@Override
	public void createOption(OptionVO vo) throws Exception {
		dao.createOption(vo);
	}

	@Override
	public List<ItemVO> listAllProceed() throws Exception {
		// TODO Auto-generated method stub
		return dao.listAllProceed();
	}

	@Override
	public List<ItemVO> listAllComing() throws Exception {
		// TODO Auto-generated method stub
		return dao.listAllComing();
	}

	@Override
	public List<ItemVO> listAllBidEnd() throws Exception {
		return dao.listAllBidEnd();
	}

	@Override
	public List<ItemVO> listAllReadyPayment() throws Exception {
		return dao.listAllReadyPayment();
	}

	@Override
	public void finishAuction(int item_id, String[] price, String[] option_id) throws Exception {
		//update item flag
		dao.finishAuction(item_id);
		//insert 최종 낙찰가

		for(int i=0;i<option_id.length;i++){
			PriceVO vo=new PriceVO();
			vo.setOption_id(Integer.parseInt(option_id[i]));
			vo.setOption_final_price(Integer.parseInt(price[i]));
			dao.insertFinalPrice(vo);
		}
	}

	@Override
	public void forceFinishAuction(int item_id) throws Exception {
		dao.finishAuction(item_id);
	}
}
