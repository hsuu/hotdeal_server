package com.alwayke.lessonap.service;

import com.alwayke.lessonap.domain.*;
import org.springframework.security.core.userdetails.User;

import java.util.List;

public interface AdminService {

 public List<UserVO> listAllUsers(int start) throws Exception;
 public UserVO getUserById(int user_id) throws Exception;
 public int countUsers() throws Exception;
 public List<UserVO> listUsersByName(String name)throws Exception;
 public void setAsArtist(int user_id) throws Exception;
 public void setPassword(ChangePasswordVO vo)throws Exception;

 public List<InquiryVO> listAllInquiry() throws Exception;
  public List<InquiryVO> listAllInquiryDone() throws Exception;

  public AnswerVO getInquiry(int inquiry_id) throws Exception;
  public void answerInquiry(AnswerVO vo) throws Exception;

  public String deposit(DepositVO vo);

  public List<PaymentVO> listPaymentByUsername(String user_name)throws Exception;

  public void paymentDone(PaymentVO vo)throws Exception;

 public List<PaymentVO> listOptionPayment(int option_id)throws Exception;

 public void sendSMS(String message)throws Exception;

}
