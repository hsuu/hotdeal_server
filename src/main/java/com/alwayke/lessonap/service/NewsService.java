package com.alwayke.lessonap.service;

import com.alwayke.lessonap.domain.*;

import java.util.List;

public interface NewsService {

 public int insertNews(NewsVO vo)throws Exception;
 public List<NewsVO> listNews()throws Exception;
 public NewsVO readNews(int news_id)throws Exception;
 public void updateNews(NewsVO vo)throws Exception;
 public void deleteNews(int news_id)throws Exception;

}
