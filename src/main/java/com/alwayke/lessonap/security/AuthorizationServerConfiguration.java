package com.alwayke.lessonap.security;

import com.alwayke.lessonap.service.UserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.web.bind.annotation.CrossOrigin;
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

	private static String REALM="MY_OAUTH_REALM";

@Autowired
private UserDetailService userDetailService;

	@Bean
	public JwtAccessTokenConverter tokenEnhancer() {

		return new JwtAccessTokenConverter();
	}
	@Bean
	public JwtTokenStore tokenStore() {
		return new JwtTokenStore(tokenEnhancer());
	}


	@Qualifier("authenticationManager")
	@Autowired
	private AuthenticationManager authenticationManager;


	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		// Which authenticationManager should be used for the password grant
		// If not provided, ResourceOwnerPasswordTokenGranter is not configured
		endpoints.authenticationManager(authenticationManager)
				.userDetailsService(userDetailService)
				.tokenStore(tokenStore())
				.accessTokenConverter(tokenEnhancer());
		// @formatter:on
	}

 	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		clients.inMemory()
				//클라이언트 아이디
	        .withClient("my-trusted-client")
				//클라이언트 시크릿
				.secret("client_secret")
				//액세스 토큰 발급 가능한 인증 타입
            .authorizedGrantTypes("password", "refresh_token")
				//접근범위
            .scopes("read", "write")
			//액세스토큰 시간
            .accessTokenValiditySeconds(60*60*24*7)//Access token is only valid for 7 days.
            //리프레시토큰 시간
				.refreshTokenValiditySeconds(60*60*24*7*12);//Refresh token is only valid for 1 year.
	}



	@Override
	public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
		oauthServer.tokenKeyAccess("isAnonymous() || hasAuthority('ROLE_USER')").checkTokenAccess("isAuthenticated()");
	}

}