package com.alwayke.lessonap.domain;

/**
 * Created by hs on 2017-06-29.
 */
public class FindIDVO {
    private String name;
    private String phone;

    public FindIDVO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
