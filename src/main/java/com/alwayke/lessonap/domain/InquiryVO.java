package com.alwayke.lessonap.domain;

import java.util.Date;

/**
 * Created by hs on 2017-05-08.
 */
public class InquiryVO {
    private int inquiry_id;
    private int user_id;
    private String inquiry_title;
    private String inquiry_content;
    private Date inquiry_indate;

    public InquiryVO() {
    }

    public Date getInquiry_indate() {
        return inquiry_indate;
    }

    public void setInquiry_indate(Date inquiry_indate) {
        this.inquiry_indate = inquiry_indate;
    }

    public int getInquiry_id() {
        return inquiry_id;
    }

    public void setInquiry_id(int inquiry_id) {
        this.inquiry_id = inquiry_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getInquiry_title() {
        return inquiry_title;
    }

    public void setInquiry_title(String inquiry_title) {
        this.inquiry_title = inquiry_title;
    }

    public String getInquiry_content() {
        return inquiry_content;
    }

    public void setInquiry_content(String inquiry_content) {
        this.inquiry_content = inquiry_content;
    }
}
