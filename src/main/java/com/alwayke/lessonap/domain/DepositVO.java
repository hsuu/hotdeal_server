package com.alwayke.lessonap.domain;

/**
 * Created by hs on 2017-05-15.
 */
public class DepositVO {
    private int payment_id;
    private int bid_id;
    private String tid;
    private String phone_number;
    private int bank_code;
    private String account;
    private int price;
    private String name;
    private String paydt;
    private String uid;
    private String charset;
    private int speed;

    public DepositVO() {
    }

    public DepositVO(String tid, String phone_number, int bank_code, String account, int price, String name, String paydt, String uid, String charset, int speed) {
        this.tid = tid;
        this.phone_number = phone_number;
        this.bank_code = bank_code;
        this.account = account;
        this.price = price;
        this.name = name;
        this.paydt = paydt;
        this.uid = uid;
        this.charset = charset;
        this.speed = speed;
    }

    public int getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(int payment_id) {
        this.payment_id = payment_id;
    }

    public int getBid_id() {
        return bid_id;
    }

    public void setBid_id(int bid_id) {
        this.bid_id = bid_id;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public int getBank_code() {
        return bank_code;
    }

    public void setBank_code(int bank_code) {
        this.bank_code = bank_code;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPaydt() {
        return paydt;
    }

    public void setPaydt(String paydt) {
        this.paydt = paydt;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }
}
