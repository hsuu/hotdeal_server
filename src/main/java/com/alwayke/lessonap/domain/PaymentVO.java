package com.alwayke.lessonap.domain;

import java.sql.Date;

/**
 * Created by hs on 2017-05-12.
 */
public class PaymentVO {
    private int bid_id;
    private int option_id;
    private int user_id;
    private int bid_price;
    private Date bid_in_date;
    private int payment_id;
    private int payment_price;
    private String user_email;
    private String user_phone;
    private String user_name;
    private int payment_bankcode;
    private String payment_paydt;
    private int bidding_bid_id;

    public PaymentVO() {
    }

    public int getBidding_bid_id() {
        return bidding_bid_id;
    }

    public void setBidding_bid_id(int bidding_bid_id) {
        this.bidding_bid_id = bidding_bid_id;
    }

    public int getBid_id() {
        return bid_id;
    }

    public void setBid_id(int bid_id) {
        this.bid_id = bid_id;
    }

    public int getOption_id() {
        return option_id;
    }

    public void setOption_id(int option_id) {
        this.option_id = option_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getBid_price() {
        return bid_price;
    }

    public void setBid_price(int bid_price) {
        this.bid_price = bid_price;
    }

    public Date getBid_in_date() {
        return bid_in_date;
    }

    public void setBid_in_date(Date bid_in_date) {
        this.bid_in_date = bid_in_date;
    }

    public int getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(int payment_id) {
        this.payment_id = payment_id;
    }

    public int getPayment_price() {
        return payment_price;
    }

    public void setPayment_price(int payment_price) {
        this.payment_price = payment_price;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public int getPayment_bankcode() {
        return payment_bankcode;
    }

    public void setPayment_bankcode(int payment_bankcode) {
        this.payment_bankcode = payment_bankcode;
    }

    public String getPayment_paydt() {
        return payment_paydt;
    }

    public void setPayment_paydt(String payment_paydt) {
        this.payment_paydt = payment_paydt;
    }
}
