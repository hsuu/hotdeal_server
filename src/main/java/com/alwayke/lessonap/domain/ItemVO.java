package com.alwayke.lessonap.domain;

import java.util.Date;
import java.util.List;
public class ItemVO {

	private int item_id;
	private Date item_in_date;
	private Date item_end_date;
	private Date item_start_date;
	private int item_max_price;
	private int item_min_price;
	private String item_main_img;
	private String item_sub_img;
	private String item_link_img;
	private String item_content;
	private String item_title;
	private String item_link;
	private int item_like;
	private int item_like_flag;
	private List<OptionVO> optionVOList;

	private ItemVO() {
	}

	public ItemVO(Date item_end_date, Date item_start_date, int item_max_price, int item_min_price, String item_content, String item_title,String item_link) {
		this.item_end_date = item_end_date;
		this.item_start_date = item_start_date;
		this.item_max_price = item_max_price;
		this.item_min_price = item_min_price;
		this.item_content = item_content;
		this.item_title = item_title;
		this.item_link=item_link;
	}

	public int getItem_like_flag() {
		return item_like_flag;
	}

	public void setItem_like_flag(int item_like_flag) {
		this.item_like_flag = item_like_flag;
	}

	public int getItem_like() {
		return item_like;
	}

	public void setItem_like(int item_like) {
		this.item_like = item_like;
	}

	public int getItem_id() {
		return item_id;
	}

	public void setItem_id(int item_id) {
		this.item_id = item_id;
	}

	public Date getItem_in_date() {
		return item_in_date;
	}

	public void setItem_in_date(Date item_in_date) {
		this.item_in_date = item_in_date;
	}

	public Date getItem_end_date() {
		return item_end_date;
	}

	public void setItem_end_date(Date item_end_date) {
		this.item_end_date = item_end_date;
	}

	public Date getItem_start_date() {
		return item_start_date;
	}

	public void setItem_start_date(Date item_start_date) {
		this.item_start_date = item_start_date;
	}

	public int getItem_max_price() {
		return item_max_price;
	}

	public void setItem_max_price(int item_max_price) {
		this.item_max_price = item_max_price;
	}

	public int getItem_min_price() {
		return item_min_price;
	}

	public void setItem_min_price(int item_min_price) {
		this.item_min_price = item_min_price;
	}

	public String getItem_main_img() {
		return item_main_img;
	}

	public void setItem_main_img(String item_main_img) {
		this.item_main_img = item_main_img;
	}

	public String getItem_sub_img() {
		return item_sub_img;
	}

	public void setItem_sub_img(String item_sub_img) {
		this.item_sub_img = item_sub_img;
	}

	public String getItem_content() {
		return item_content;
	}

	public void setItem_content(String item_content) {
		this.item_content = item_content;
	}

	public String getItem_title() {
		return item_title;
	}

	public void setItem_title(String item_title) {
		this.item_title = item_title;
	}

	public List<OptionVO> getOptionVOList() {
		return optionVOList;
	}

	public void setOptionVOList(List<OptionVO> optionVOList) {
		this.optionVOList = optionVOList;
	}

	public String getItem_link() {
		return item_link;
	}

	public void setItem_link(String item_link) {
		this.item_link = item_link;
	}

	public String getItem_link_img() {
		return item_link_img;
	}

	public void setItem_link_img(String item_link_img) {
		this.item_link_img = item_link_img;
	}
}
