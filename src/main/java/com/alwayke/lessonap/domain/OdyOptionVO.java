package com.alwayke.lessonap.domain;

/**
 * Created by hs on 2017-05-30.
 */
public class OdyOptionVO {

    private int oneday_id;
    private int ody_option_id;
    private String ody_option_name;
    private int ody_option_num;

    public OdyOptionVO() {
    }

    public OdyOptionVO(int oneday_id, String ody_option_name, int ody_option_num) {
        this.oneday_id = oneday_id;
        this.ody_option_name = ody_option_name;
        this.ody_option_num = ody_option_num;
    }

    public int getOdy_option_num() {
        return ody_option_num;
    }

    public void setOdy_option_num(int ody_option_num) {
        this.ody_option_num = ody_option_num;
    }

    public int getOneday_id() {
        return oneday_id;
    }

    public void setOneday_id(int oneday_id) {
        this.oneday_id = oneday_id;
    }

    public int getOdy_option_id() {
        return ody_option_id;
    }

    public void setOdy_option_id(int ody_option_id) {
        this.ody_option_id = ody_option_id;
    }

    public String getOdy_option_name() {
        return ody_option_name;
    }

    public void setOdy_option_name(String ody_option_name) {
        this.ody_option_name = ody_option_name;
    }
}
