package com.alwayke.lessonap.domain;

import java.util.Date;

/**
 * Created by hs on 2017-06-13.
 */
public class PhoneVO {

    private String new_phone;
    private String old_phone;
    private int user_id;
    private Date change_date;

    public String getNew_phone() {
        return new_phone;
    }

    public void setNew_phone(String new_phone) {
        this.new_phone = new_phone;
    }

    public String getOld_phone() {
        return old_phone;
    }

    public void setOld_phone(String old_phone) {
        this.old_phone = old_phone;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public Date getChange_date() {
        return change_date;
    }

    public void setChange_date(Date change_date) {
        this.change_date = change_date;
    }
}
