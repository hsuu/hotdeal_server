package com.alwayke.lessonap.domain;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class LoginUser implements UserDetails {

    private static final long serialVersionUID = -1286609632181552601L;

    private Integer user_id;
    private String user_email;
    private String password;
    private List<String> roles;
    private String isAdmin;
    private String user_name;


    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        //user id 반환
        return user_id+"";
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(String isAdmin) {
        this.isAdmin = isAdmin;
    }

    public LoginUser(){
    }

    public LoginUser(UserDTO userDto) {
        this.user_id = userDto.getUser_id();
        this.user_email = userDto.getUser_email();
        this.user_name=userDto.getUser_name();
        this.password = userDto.getUser_password();
        this.isAdmin="N";
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String role : roles) {
            authorities.add(new SimpleGrantedAuthority(role));
        }

        return authorities;
    }
}
