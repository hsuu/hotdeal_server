package com.alwayke.lessonap.domain;

/**
 * Created by hs on 2017-06-13.
 */
public class AccountVO {
    private int user_id;
    //예금주
    private String account_holder;
    //계좌번호
    private String account_number;
    //은행
    private String account_bank;

    public String getAccount_bank() {
        return account_bank;
    }

    public void setAccount_bank(String account_bank) {
        this.account_bank = account_bank;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getAccount_holder() {
        return account_holder;
    }

    public void setAccount_holder(String account_holder) {
        this.account_holder = account_holder;
    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }
}
