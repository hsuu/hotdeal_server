package com.alwayke.lessonap.domain;

/**
 * Created by hs on 2017-05-04.
 */
public class ImageVO {
    private int item_id;
    private String item_main_img;
    private String item_sub_img;
    private String item_link_img;

    public ImageVO(int item_id,String item_main_img, String item_sub_img) {
        this.item_id=item_id;
        this.item_main_img = item_main_img;
        this.item_sub_img = item_sub_img;
    }

    public ImageVO(int item_id,String item_main_img, String item_sub_img,String item_link_img) {
        this.item_id=item_id;
        this.item_main_img = item_main_img;
        this.item_sub_img = item_sub_img;
        this.item_link_img=item_link_img;
    }

    public int getItem_id() {
        return item_id;
    }
    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public String getItem_main_img() {
        return item_main_img;
    }

    public void setItem_main_img(String item_main_img) {
        this.item_main_img = item_main_img;
    }

    public String getItem_sub_img() {
        return item_sub_img;
    }

    public void setItem_sub_img(String item_sub_img) {
        this.item_sub_img = item_sub_img;
    }

    public String getItem_link_img() {
        return item_link_img;
    }

    public void setItem_link_img(String item_link_img) {
        this.item_link_img = item_link_img;
    }
}
