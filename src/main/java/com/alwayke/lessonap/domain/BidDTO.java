package com.alwayke.lessonap.domain;

/**
 * Created by hs on 2017-05-01.
 */
public class BidDTO {
    private int option_id;
    private int user_id;
    private int bid_price;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getOption_id() {
        return option_id;
    }

    public void setOption_id(int option_id) {
        this.option_id = option_id;
    }

    public int getBid_price() {
        return bid_price;
    }

    public void setBid_price(int bid_price) {
        this.bid_price = bid_price;
    }
}
