package com.alwayke.lessonap.domain;

import java.util.Date;

/**
 * Created by hs on 2017-05-31.
 */
public class TicPurchaseDTO {
    private int tic_option_id;
    private int tic_purchase_id;
    private int user_id;
    private String user_email;
    private String user_phone;
    private Date tic_purchase_indate;
    private int tic_purchase_flag;

    public TicPurchaseDTO() {
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }

    public int getTic_option_id() {
        return tic_option_id;
    }

    public void setTic_option_id(int tic_option_id) {
        this.tic_option_id = tic_option_id;
    }

    public int getTic_purchase_id() {
        return tic_purchase_id;
    }

    public void setTic_purchase_id(int tic_purchase_id) {
        this.tic_purchase_id = tic_purchase_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public Date getTic_purchase_indate() {
        return tic_purchase_indate;
    }

    public void setTic_purchase_indate(Date tic_purchase_indate) {
        this.tic_purchase_indate = tic_purchase_indate;
    }

    public int getTic_purchase_flag() {
        return tic_purchase_flag;
    }

    public void setTic_purchase_flag(int tic_purchase_flag) {
        this.tic_purchase_flag = tic_purchase_flag;
    }
}
