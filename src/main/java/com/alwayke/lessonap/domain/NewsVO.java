package com.alwayke.lessonap.domain;

import java.util.Date;

/**
 * Created by hs on 2017-06-27.
 */
public class NewsVO {
    private int news_id;
    private String news_title;
    private String news_content;
    private Date news_indate;

    public NewsVO() {
    }

    public NewsVO(String news_title, String news_content) {
        this.news_title = news_title;
        this.news_content = news_content;
    }

    public NewsVO(int news_id, String news_title, String news_content) {
        this.news_id = news_id;
        this.news_title = news_title;
        this.news_content = news_content;
    }

    public int getNews_id() {
        return news_id;
    }

    public void setNews_id(int news_id) {
        this.news_id = news_id;
    }

    public String getNews_title() {
        return news_title;
    }

    public void setNews_title(String news_title) {
        this.news_title = news_title;
    }

    public String getNews_content() {
        return news_content;
    }

    public void setNews_content(String news_content) {
        this.news_content = news_content;
    }

    public Date getNews_indate() {
        return news_indate;
    }

    public void setNews_indate(Date news_indate) {
        this.news_indate = news_indate;
    }
}
