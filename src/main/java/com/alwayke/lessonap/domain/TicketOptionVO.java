package com.alwayke.lessonap.domain;

/**
 * Created by hs on 2017-05-30.
 */
public class TicketOptionVO {

    private int ticket_id;
    private int tic_option_id;
    private String tic_option_name;
    private int tic_option_num;

    public TicketOptionVO() {
    }

    public TicketOptionVO(int ticket_id, String tic_option_name, int tic_option_num) {
        this.ticket_id = ticket_id;
        this.tic_option_name = tic_option_name;
        this.tic_option_num = tic_option_num;
    }

    public int getTic_option_num() {
        return tic_option_num;
    }

    public void setTic_option_num(int tic_option_num) {
        this.tic_option_num = tic_option_num;
    }

    public int getTicket_id() {
        return ticket_id;
    }

    public void setTicket_id(int ticket_id) {
        this.ticket_id = ticket_id;
    }

    public int getTic_option_id() {
        return tic_option_id;
    }

    public void setTic_option_id(int tic_option_id) {
        this.tic_option_id = tic_option_id;
    }

    public String getTic_option_name() {
        return tic_option_name;
    }

    public void setTic_option_name(String tic_option_name) {
        this.tic_option_name = tic_option_name;
    }
}
