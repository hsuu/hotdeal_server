package com.alwayke.lessonap.domain;

import java.util.Date;
import java.util.List;

/**
 * Created by hs on 2017-05-30.
 */
public class OnedayVO {
    private int oneday_id;
    private Date oneday_in_date;
    private Date oneday_end_date;
    private Date oneday_start_date;
    private int oneday_price;
    private String oneday_main_img;
    private String oneday_sub_img;
    private String oneday_content;
    private String oneday_title;
    private List<OdyOptionVO> odyOptionVOList;

    public OnedayVO() {
    }

    public OnedayVO(Date oneday_end_date, Date oneday_start_date, int oneday_price, String oneday_content, String oneday_title) {
        this.oneday_end_date = oneday_end_date;
        this.oneday_start_date = oneday_start_date;
        this.oneday_price = oneday_price;
        this.oneday_content = oneday_content;
        this.oneday_title = oneday_title;
    }

    public int getOneday_id() {
        return oneday_id;
    }

    public void setOneday_id(int oneday_id) {
        this.oneday_id = oneday_id;
    }

    public Date getOneday_in_date() {
        return oneday_in_date;
    }

    public void setOneday_in_date(Date oneday_in_date) {
        this.oneday_in_date = oneday_in_date;
    }

    public Date getOneday_end_date() {
        return oneday_end_date;
    }

    public void setOneday_end_date(Date oneday_end_date) {
        this.oneday_end_date = oneday_end_date;
    }

    public Date getOneday_start_date() {
        return oneday_start_date;
    }

    public void setOneday_start_date(Date oneday_start_date) {
        this.oneday_start_date = oneday_start_date;
    }

    public int getOneday_price() {
        return oneday_price;
    }

    public void setOneday_price(int oneday_price) {
        this.oneday_price = oneday_price;
    }

    public String getOneday_main_img() {
        return oneday_main_img;
    }

    public void setOneday_main_img(String oneday_main_img) {
        this.oneday_main_img = oneday_main_img;
    }

    public String getOneday_sub_img() {
        return oneday_sub_img;
    }

    public void setOneday_sub_img(String oneday_sub_img) {
        this.oneday_sub_img = oneday_sub_img;
    }

    public String getOneday_content() {
        return oneday_content;
    }

    public void setOneday_content(String oneday_content) {
        this.oneday_content = oneday_content;
    }

    public String getOneday_title() {
        return oneday_title;
    }

    public void setOneday_title(String oneday_title) {
        this.oneday_title = oneday_title;
    }

    public List<OdyOptionVO> getOdyOptionVOList() {
        return odyOptionVOList;
    }

    public void setOdyOptionVOList(List<OdyOptionVO> odyOptionVOList) {
        this.odyOptionVOList = odyOptionVOList;
    }
}
