package com.alwayke.lessonap.domain;

import java.util.Date;
import java.util.List;

/**
 * Created by hs on 2017-05-30.
 */
public class TicketVO {
    private int ticket_id;
    private Date ticket_in_date;
    private Date ticket_end_date;
    private Date ticket_start_date;
    private int ticket_price;
    private String ticket_main_img;
    private String ticket_sub_img;
    private String ticket_content;
    private String ticket_title;
    private List<TicketOptionVO> ticketOptionVOList;

    public TicketVO() {
    }

    public TicketVO(Date ticket_end_date, Date ticket_start_date, int ticket_price, String ticket_content, String ticket_title) {
        this.ticket_end_date = ticket_end_date;
        this.ticket_start_date = ticket_start_date;
        this.ticket_price = ticket_price;
        this.ticket_content = ticket_content;
        this.ticket_title = ticket_title;
    }

    public int getTicket_id() {
        return ticket_id;
    }

    public void setTicket_id(int ticket_id) {
        this.ticket_id = ticket_id;
    }

    public Date getTicket_in_date() {
        return ticket_in_date;
    }

    public void setTicket_in_date(Date ticket_in_date) {
        this.ticket_in_date = ticket_in_date;
    }

    public Date getTicket_end_date() {
        return ticket_end_date;
    }

    public void setTicket_end_date(Date ticket_end_date) {
        this.ticket_end_date = ticket_end_date;
    }

    public Date getTicket_start_date() {
        return ticket_start_date;
    }

    public void setTicket_start_date(Date ticket_start_date) {
        this.ticket_start_date = ticket_start_date;
    }

    public int getTicket_price() {
        return ticket_price;
    }

    public void setTicket_price(int ticket_price) {
        this.ticket_price = ticket_price;
    }

    public String getTicket_main_img() {
        return ticket_main_img;
    }

    public void setTicket_main_img(String ticket_main_img) {
        this.ticket_main_img = ticket_main_img;
    }

    public String getTicket_sub_img() {
        return ticket_sub_img;
    }

    public void setTicket_sub_img(String ticket_sub_img) {
        this.ticket_sub_img = ticket_sub_img;
    }

    public String getTicket_content() {
        return ticket_content;
    }

    public void setTicket_content(String ticket_content) {
        this.ticket_content = ticket_content;
    }

    public String getTicket_title() {
        return ticket_title;
    }

    public void setTicket_title(String ticket_title) {
        this.ticket_title = ticket_title;
    }

    public List<TicketOptionVO> getTicketOptionVOList() {
        return ticketOptionVOList;
    }

    public void setTicketOptionVOList(List<TicketOptionVO> ticketOptionVOList) {
        this.ticketOptionVOList = ticketOptionVOList;
    }
}
