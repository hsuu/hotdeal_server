package com.alwayke.lessonap.domain;

/**
 * Created by hs on 2017-05-02.
 */
public class OptionVO {

    private int item_id;
    private int option_id;
    private String option_name;
    private int option_min_num;
    private int bid_count;
    private int option_final_price;

    private OptionVO(){}

    public OptionVO(int item_id, String option_name, int option_min_num) {
        this.item_id = item_id;
        this.option_name = option_name;
        this.option_min_num = option_min_num;
    }
    public OptionVO(int item_id, String option_name, int option_min_num,int option_final_price) {
        this.item_id = item_id;
        this.option_name = option_name;
        this.option_min_num = option_min_num;
        this.option_final_price=option_final_price;
    }

    public int getOption_final_price() {
        return option_final_price;
    }

    public void setOption_final_price(int option_final_price) {
        this.option_final_price = option_final_price;
    }

    public int getBid_count() {
        return bid_count;
    }

    public void setBid_count(int bid_count) {
        this.bid_count = bid_count;
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public int getOption_id() {
        return option_id;
    }

    public void setOption_id(int option_id) {
        this.option_id = option_id;
    }

    public String getOption_name() {
        return option_name;
    }

    public void setOption_name(String option_name) {
        this.option_name = option_name;
    }

    public int getOption_min_num() {
        return option_min_num;
    }

    public void setOption_min_num(int option_min_num) {
        this.option_min_num = option_min_num;
    }
}
