package com.alwayke.lessonap.domain;

import java.util.Date;

public class BidVO {

	private int bid_id;
	private int option_id;
	private int user_id;
	private int bid_price;
	private int bid_flag;
	private Date bid_in_date;
	private Date bid_cancel_date;
	private String user_email;
	private int item_id;
	private String item_title;
	private String option_name;

	public int getItem_id() {
		return item_id;
	}

	public String getOption_name() {
		return option_name;
	}

	public void setOption_name(String option_name) {
		this.option_name = option_name;
	}

	public void setItem_id(int item_id) {
		this.item_id = item_id;
	}

	public String getItem_title() {
		return item_title;
	}

	public void setItem_title(String item_title) {
		this.item_title = item_title;
	}

	public Date getBid_in_date() {
		return bid_in_date;
	}
	public Date getBid_cancel_date() {
		return bid_cancel_date;
	}

	public void setBid_in_date(Date bid_in_date) {
		this.bid_in_date = bid_in_date;
	}

	public void setBid_cancel_date(Date bid_cancel_date) {
		this.bid_cancel_date = bid_cancel_date;
	}

	public String getUser_email() {
		return user_email;
	}

	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}

	public int getBid_id() {
		return bid_id;
	}
	public void setBid_id(int bid_id) {
		this.bid_id = bid_id;
	}
	public int getOption_id() {
		return option_id;
	}
	public void setOption_id(int option_id) {
		this.option_id = option_id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getBid_price() {
		return bid_price;
	}
	public void setBid_price(int bid_price) {
		this.bid_price = bid_price;
	}
	public int getBid_flag() {
		return bid_flag;
	}
	public void setBid_flag(int bid_flag) {
		this.bid_flag = bid_flag;
	}
	
	
}
