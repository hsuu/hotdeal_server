package com.alwayke.lessonap.domain;

/**
 * Created by hs on 2017-06-28.
 */
public class LikeVO {

    private int like_id;
    private int item_id;
    private int user_id;

    public LikeVO() {
    }

    public LikeVO(int item_id, int user_id) {
        this.item_id = item_id;
        this.user_id = user_id;
    }

    public int getLike_id() {
        return like_id;
    }

    public void setLike_id(int like_id) {
        this.like_id = like_id;
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

}
