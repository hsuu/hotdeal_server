package com.alwayke.lessonap.domain;

/**
 * Created by hs on 2017-06-19.
 */
public class ChangePasswordVO {
    private int user_id;
    private String user_password;
    private String user_birth;

    public ChangePasswordVO(int user_id, String user_birth) {
        this.user_id = user_id;
        this.user_birth = user_birth;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUser_password() {
        return user_password;
    }

    public void setUser_password(String user_password) {
        this.user_password = user_password;
    }

    public String getUser_birth() {
        return user_birth;
    }

    public void setUser_birth(String user_birth) {
        this.user_birth = user_birth;
    }
}
