package com.alwayke.lessonap.domain;

import java.util.Date;

public class UserVO {
	
	private int user_id;
	private String user_email;
	private String user_password;
	private String user_name;
	private String user_phone;
	private String user_birth;
	private String user_gender;
	private int user_sms;
	private int user_marketing;
	private Date user_in_date;
	private int user_artist;

	public int getUser_artist() {
		return user_artist;
	}

	public void setUser_artist(int user_artist) {
		this.user_artist = user_artist;
	}

	public Date getUser_in_date() {
		return user_in_date;
	}

	public void setUser_in_date(Date user_in_date) {
		this.user_in_date = user_in_date;
	}

	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getUser_email() {
		return user_email;
	}
	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}
	public String getUser_password() {
		return user_password;
	}
	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getUser_phone() {
		return user_phone;
	}
	public void setUser_phone(String user_phone) {
		this.user_phone = user_phone;
	}
	public String getUser_birth() {
		return user_birth;
	}
	public void setUser_birth(String user_birth) {
		this.user_birth = user_birth;
	}
	public String getUser_gender() {
		return user_gender;
	}
	public void setUser_gender(String user_gender) {
		this.user_gender = user_gender;
	}
	public int getUser_sms() {
		return user_sms;
	}
	public void setUser_sms(int user_sms) {
		this.user_sms = user_sms;
	}
	public int getUser_marketing() {
		return user_marketing;
	}
	public void setUser_marketing(int user_marketing) {
		this.user_marketing = user_marketing;
	}
	
}
