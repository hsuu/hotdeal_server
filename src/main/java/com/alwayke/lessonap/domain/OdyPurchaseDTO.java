package com.alwayke.lessonap.domain;

import java.util.Date;

/**
 * Created by hs on 2017-05-31.
 */
public class OdyPurchaseDTO {
    private int ody_option_id;
    private int ody_purchase_id;
    private int user_id;
    private String user_email;
    private String user_phone;
    private Date ody_purchase_indate;
    private int ody_purchase_flag;

    public OdyPurchaseDTO() {
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }

    public int getOdy_option_id() {
        return ody_option_id;
    }

    public void setOdy_option_id(int ody_option_id) {
        this.ody_option_id = ody_option_id;
    }

    public int getOdy_purchase_id() {
        return ody_purchase_id;
    }

    public void setOdy_purchase_id(int ody_purchase_id) {
        this.ody_purchase_id = ody_purchase_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public Date getOdy_purchase_indate() {
        return ody_purchase_indate;
    }

    public void setOdy_purchase_indate(Date ody_purchase_indate) {
        this.ody_purchase_indate = ody_purchase_indate;
    }

    public int getOdy_purchase_flag() {
        return ody_purchase_flag;
    }

    public void setOdy_purchase_flag(int ody_purchase_flag) {
        this.ody_purchase_flag = ody_purchase_flag;
    }
}
