package com.alwayke.lessonap.domain;

/**
 * Created by hs on 2017-05-11.
 */
public class PriceVO {
    private int option_id;
    private int option_final_price;

    public PriceVO() {
    }

    public int getOption_id() {
        return option_id;
    }

    public void setOption_id(int option_id) {
        this.option_id = option_id;
    }

    public int getOption_final_price() {
        return option_final_price;
    }

    public void setOption_final_price(int option_final_price) {
        this.option_final_price = option_final_price;
    }
}
