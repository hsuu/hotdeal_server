package com.alwayke.lessonap.persistence;

import com.alwayke.lessonap.domain.*;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by hs on 2017-05-30.
 */
@Repository
public class OnedayDAOImpl implements OnedayDAO{

    @Inject
    private SqlSession session;
    private static String namespace = "com.alwayke.lessonap.mapper.onedayMapper";

    @Override
    public OnedayVO read(int oneday_id) throws Exception {
        return session.selectOne(namespace+".read",oneday_id);
    }

    @Override
    public List<OdyOptionVO> readOptions(int oneday_id) throws Exception {
        return session.selectList(namespace+".readOptions",oneday_id);
    }

    @Override
    public int create(OnedayVO vo) throws Exception {
        session.insert(namespace + ".create", vo);

        return vo.getOneday_id();
    }

    @Override
    public void createImg(ImageVO vo) throws Exception {
        session.update(namespace+".createImg",vo);
    }

    @Override
    public void createOption(OdyOptionVO vo) throws Exception {
        session.insert(namespace+".createOption",vo);
    }

    @Override
    public List<OnedayVO> listAllProceed() throws Exception {
        return session.selectList(namespace + ".listAllProceed");
    }

    @Override
    public List<OnedayVO> listAllComing() throws Exception {
        return session.selectList(namespace + ".listAllComing");
    }

    @Override
    public List<OnedayVO> listAllEnd() throws Exception {
        return session.selectList(namespace+".listAllEnd");
    }

    @Override
    public List<OdyPurchaseDTO> listAllPurchase(int ody_option_id) throws Exception {
        return session.selectList(namespace+".listAllPurchase",ody_option_id);
    }

    @Override
    public int checkPurchase(int ody_option_id) throws Exception {
        return session.selectOne(namespace+".checkPurchase",ody_option_id);
    }

    @Override
    public void purchaseOneday(OdyPurchaseVO vo) throws Exception {
        session.insert(namespace+".purchaseOneday",vo);
    }

    @Override
    public void updatePurchaseFlag(int ody_purchase_id) throws Exception {
        session.update(namespace+".updatePurchaseFlag",ody_purchase_id);
    }

}
