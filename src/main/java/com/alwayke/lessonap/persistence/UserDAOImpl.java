package com.alwayke.lessonap.persistence;

import com.alwayke.lessonap.domain.*;
import org.apache.ibatis.session.SqlSession;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.List;


@Repository
public class UserDAOImpl implements UserDAO {

	@Inject
	private SqlSession session;

	private static String namespace = "com.alwayke.lessonap.mapper.userMapper";

	@Override
	public void regist(UserVO vo) {
		// TODO Auto-generated method stub
		
		session.insert(namespace + ".regist", vo);
	}

	@Override
	public UserDTO select(String user_name) throws UsernameNotFoundException {
		return session.selectOne(namespace+".selectName",user_name);
	}

	@Override
	public UserVO getUser(int user_id) throws Exception {
		return session.selectOne(namespace+".getUser",user_id);
	}

	@Override
	public void inquiry(InquiryVO vo) throws Exception {
		session.insert(namespace+".insertInquiry",vo);
	}

	@Override
	public List<InquiryVO> listInquiryWaiting(int user_id) throws Exception {
		return session.selectList(namespace+".listInquiryWaiting",user_id);
	}

	@Override
	public List<InquiryVO> listInquiryEnd(int user_id) throws Exception {
		return session.selectList(namespace+".listInquiryEnd",user_id);
	}

	@Override
	public void updatePhone(PhoneVO vo) throws Exception {
		session.update(namespace+".updatePhone",vo);
	}

	@Override
	public void insertPhoneLog(PhoneVO vo) throws Exception {
		session.insert(namespace+".insertPhoneLog",vo);

	}

	@Override
	public void insertAccount(int user_id) throws Exception {
		session.insert(namespace+".insertAccount",user_id);
	}

	@Override
	public AccountVO getUserAccount(int user_id) throws Exception {
		return session.selectOne(namespace+".getUserAccount",user_id);
	}

	@Override
	public void updateAccount(AccountVO vo) throws Exception {
		session.update(namespace+".updateAccount",vo);
	}

	@Override
	public String findId(FindIDVO vo) throws Exception {
		return session.selectOne(namespace+".findId",vo);
	}

	@Override
	public UserVO findPassword(FindIDVO vo) throws Exception {
		return session.selectOne(namespace+".findPassword",vo);
	}
}
