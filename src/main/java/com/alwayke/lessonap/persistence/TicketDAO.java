package com.alwayke.lessonap.persistence;

import com.alwayke.lessonap.domain.*;

import java.util.List;

public interface TicketDAO {

	public TicketVO read(int ticket_id) throws Exception;

	public List<TicketOptionVO> readOptions(int ticket_id) throws Exception;

	public int create(TicketVO vo) throws Exception;
	public void createImg(ImageVO vo)throws Exception;

	public void createOption(TicketOptionVO vo)throws Exception;

	public List<TicketVO> listAllProceed() throws Exception;
	public List<TicketVO> listAllComing() throws Exception;
	public List<TicketVO> listAllEnd() throws Exception;

	public List<TicPurchaseDTO> listAllPurchase(int tic_option_id) throws Exception;
	public int checkPurchase(int tic_option_id)throws Exception;
	public void purchaseTicket(TicPurchaseVO vo)throws Exception;
	public void updatePurchaseFlag(int tic_purchase_id)throws Exception;

}
