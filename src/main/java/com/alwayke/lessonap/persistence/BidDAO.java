package com.alwayke.lessonap.persistence;

import java.util.List;

import com.alwayke.lessonap.domain.BidDTO;
import com.alwayke.lessonap.domain.BidVO;

public interface BidDAO {

	public int checkBidding(BidDTO dto) throws Exception;

	public void bidding(BidDTO dto) throws Exception;
	
	public void biddingCancel(int bid_id) throws Exception;
	
	public List<BidVO> listAllBidding(int user_id) throws Exception;

	public List<BidVO> listAllCancelBid(int user_id) throws Exception;

	public List<BidVO> listAllBidOnItem(int user_id,int item_id) throws Exception;
	public List<BidVO> listAllBidOnOption(int option_id) throws Exception;
	public int countBidOnOption(int option_id) throws Exception;
}
