package com.alwayke.lessonap.persistence;

import com.alwayke.lessonap.domain.*;

import java.util.List;

public interface NoticeDAO {

	public void insertNotice(NoticeVO vo)throws Exception;
	public List<NoticeVO> listNotice()throws Exception;
	public NoticeVO readNotice(int notice_id)throws Exception;
	public void updateNotice(NoticeVO vo)throws Exception;
	public void deleteNotice(int notice_id)throws Exception;

}
