package com.alwayke.lessonap.persistence;

import com.alwayke.lessonap.domain.*;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public class AdminDAOImpl implements AdminDAO{

	@Autowired
	private SqlSession session;
	private static String namespace = "com.alwayke.lessonap.mapper.adminMapper";

	@Override
	public List<UserVO> listAllUsers(int start) throws Exception {
		return session.selectList(namespace+".listAllUsers",start);
	}

	@Override
	public UserVO getUserById(int user_id) throws Exception {
		return session.selectOne(namespace+".getUserById",user_id);
	}

	@Override
	public int countUsers() throws Exception {
		return session.selectOne(namespace+".countUsers");
	}

	@Override
	public List<UserVO> listUsersByName(String name) throws Exception {
		return session.selectList(namespace+".listUsersByName",name);
	}

	@Override
	public void setAsArtist(int user_id) throws Exception {
		session.update(namespace+".setAsArtist",user_id);
	}

	@Override
	public void setPassword(ChangePasswordVO vo) throws Exception {
		session.update(namespace+".setPassword",vo);
	}

	@Override
	public List<InquiryVO> listAllInquiry() throws Exception {
		return session.selectList(namespace+".listAllInquiry");
	}

	@Override
	public List<InquiryVO> listAllInquiryDone() throws Exception {
		return session.selectList(namespace+".listAllInquiryDone");
	}

	@Override
	public AnswerVO getInquiry(int inquiry_id) throws Exception {
		return session.selectOne(namespace+".getInquiry",inquiry_id);
	}

	@Override
	public void answerInquiry(int inquiry_id) throws Exception {
		session.update(namespace+".answerInquiry",inquiry_id);
	}

	@Override
	public void insertAnswer(AnswerVO vo) throws Exception {
		session.insert(namespace+".insertAnswer",vo);
	}

	@Override
	public int checkPaymentTid(String tid) throws Exception {
		return session.selectOne(namespace+".checkPaymentTid",tid);
	}

	@Override
	public void insertPayment(DepositVO vo) throws Exception {
		session.insert(namespace+".insertPayment",vo);
	}

	@Override
	public List<PaymentVO> listPaymentByUsername(String user_name) throws Exception {
		return session.selectList(namespace+".listPaymentByUsername",user_name);
	}

	@Override
	public void paymentDone(PaymentVO vo) throws Exception {
		session.update(namespace+".paymentDone",vo);
	}
	@Override
	public List<PaymentVO> listOptionPayment(int option_id) throws Exception {
		return session.selectList(namespace+".listOptionPayment",option_id);
	}

	@Override
	public List<String> listSMSandMarketing() throws Exception {
		return session.selectList(namespace+".listSMSandMarketing");
	}
}
