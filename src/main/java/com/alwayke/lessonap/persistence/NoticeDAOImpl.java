package com.alwayke.lessonap.persistence;

import com.alwayke.lessonap.domain.NoticeVO;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class NoticeDAOImpl implements NoticeDAO{

	@Autowired
	private SqlSession session;
	private static String namespace = "com.alwayke.lessonap.mapper.noticeMapper";

	@Override
	public void insertNotice(NoticeVO vo) throws Exception {
		session.insert(namespace+".insertNotice",vo);
	}

	@Override
	public List<NoticeVO> listNotice() throws Exception {
		return session.selectList(namespace+".listNotice");
	}

	@Override
	public NoticeVO readNotice(int notice_id) throws Exception {
		return session.selectOne(namespace+".readNotice",notice_id);
	}

	@Override
	public void updateNotice(NoticeVO vo) throws Exception {
		session.update(namespace+".updateNotice",vo);
	}

	@Override
	public void deleteNotice(int notice_id) throws Exception {
		session.delete(namespace+".deleteNotice",notice_id);
	}
}
