package com.alwayke.lessonap.persistence;

import com.alwayke.lessonap.domain.*;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public class NewsDAOImpl implements NewsDAO{

	@Autowired
	private SqlSession session;
	private static String namespace = "com.alwayke.lessonap.mapper.newsMapper";


	@Override
	public int insertNews(NewsVO vo) throws Exception {
		session.insert(namespace+".insertNews",vo);
		return vo.getNews_id();
	}

	@Override
	public List<NewsVO> listNews() throws Exception {
		return session.selectList(namespace+".listNews");
	}

	@Override
	public NewsVO readNews(int news_id) throws Exception {
		return session.selectOne(namespace+".readNews",news_id);
	}

	@Override
	public void updateNews(NewsVO vo) throws Exception {
		session.update(namespace+".updateNews",vo);
	}

	@Override
	public void deleteNews(int news_id) throws Exception {
		session.delete(namespace+".deleteNews",news_id);
	}
}
