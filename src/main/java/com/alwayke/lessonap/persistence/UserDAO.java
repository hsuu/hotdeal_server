package com.alwayke.lessonap.persistence;

import com.alwayke.lessonap.domain.*;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.List;

public interface UserDAO {

	public void regist(UserVO vo) throws Exception;
	
	public UserVO getUser(int user_id) throws Exception;

	public UserDTO select(String user_name) throws UsernameNotFoundException;

	public void inquiry(InquiryVO vo)throws Exception;

	public List<InquiryVO> listInquiryWaiting(int user_id) throws Exception;
	public List<InquiryVO> listInquiryEnd(int user_id) throws Exception;

	public void updatePhone(PhoneVO vo)throws Exception;
	public void insertPhoneLog(PhoneVO vo)throws Exception;
	public void insertAccount(int user_id)throws Exception;
	public AccountVO getUserAccount(int user_id)throws Exception;
	public void updateAccount(AccountVO vo)throws Exception;

	public String findId(FindIDVO vo)throws Exception;
	public UserVO findPassword(FindIDVO vo)throws Exception;
}
