package com.alwayke.lessonap.persistence;

import java.util.List;

import javax.inject.Inject;

import com.alwayke.lessonap.domain.*;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

@Repository
public class ItemDAOImpl implements ItemDAO {

	@Inject
	private SqlSession session;
	private static String namespace = "com.alwayke.lessonap.mapper.itemMapper";

	private static String namespace2 = "com.alwayke.lessonap.mapper.bidMapper";
	@Override
	public int create(ItemVO vo) throws Exception {
		// TODO Auto-generated method stub

		session.insert(namespace + ".create", vo);
		
		return vo.getItem_id();
		
	}
	@Override
	public void createImg(ImageVO vo) throws Exception {
		session.update(namespace+".createImg",vo);
	}

	@Override
	public void createOption(OptionVO vo) throws Exception {
		session.insert(namespace+".createOption",vo);
	}

	@Override
	public ItemVO read(int item_id) throws Exception {
		// TODO Auto-generated method stub
		return session.selectOne(namespace + ".read", item_id);
	}

	@Override
	public List<OptionVO> readOptions(int item_id) throws Exception {
		return session.selectList(namespace+".readOptions",item_id);
	}

	@Override
	public List<ItemVO> listAllProceed() throws Exception {
		// TODO Auto-generated method stub
		
		return session.selectList(namespace + ".listAllProceed");
	}

	@Override
	public List<ItemVO> listAllComing() throws Exception {
		// TODO Auto-generated method stub
		return session.selectList(namespace + ".listAllComing");
	}

	@Override
	public List<ItemVO> listAllBidEnd() throws Exception {
		return session.selectList(namespace+".listAllBidEnd");
	}

	@Override
	public List<ItemVO> listAllReadyPayment() throws Exception {
		return session.selectList(namespace+".listAllReadyPayment");
	}

	@Override
	public void finishAuction(int item_id) throws Exception {
		session.update(namespace+".finishAuction",item_id);
	}
	@Override
	public void insertFinalPrice(PriceVO vo) throws Exception {
		session.insert(namespace+".insertFinalPrice",vo);
	}



}
