package com.alwayke.lessonap.persistence;

import com.alwayke.lessonap.domain.LikeVO;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
/**
 * Created by hs on 2017-06-28.
 */
@Repository
public class LikeDAOImpl implements LikeDAO {

    @Autowired
    private SqlSession session;
    private static String namespace = "com.alwayke.lessonap.mapper.likeMapper";

    @Override
    public void insertLike(LikeVO vo) throws Exception {
        session.insert(namespace+".insertLike",vo);
    }

    @Override
    public void deleteLike(LikeVO vo) throws Exception {
        session.delete(namespace+".deleteLike",vo);
    }

    @Override
    public int checkLike(LikeVO vo) throws Exception {
        return session.selectOne(namespace+".checkLike",vo);
    }

    @Override
    public void increaseLike(int item_id) throws Exception {
        session.update(namespace+".increaseLike",item_id);
    }

    @Override
    public void decreaseLike(int item_id) throws Exception {
        session.update(namespace+".decreaseLike",item_id);
    }
}
