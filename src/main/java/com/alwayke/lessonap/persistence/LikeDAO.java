package com.alwayke.lessonap.persistence;

import com.alwayke.lessonap.domain.LikeVO;

/**
 * Created by hs on 2017-06-28.
 */
public interface LikeDAO {

    public void insertLike(LikeVO vo)throws Exception;
    public void deleteLike(LikeVO vo)throws Exception;

    public int checkLike(LikeVO vo)throws Exception;

    public void increaseLike(int item_id)throws Exception;
    public void decreaseLike(int item_id)throws Exception;

}
