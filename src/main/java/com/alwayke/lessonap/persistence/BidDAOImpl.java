package com.alwayke.lessonap.persistence;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import com.alwayke.lessonap.domain.BidDTO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.alwayke.lessonap.domain.BidVO;

@Repository
public class BidDAOImpl implements BidDAO {

	@Inject
	private SqlSession session;
	private static String namespace = "com.alwayke.lessonap.mapper.bidMapper";

	@Override
	public List<BidVO> listAllBidOnItem(int user_id, int item_id) throws Exception {
		return null;
	}

	@Override
	public int checkBidding(BidDTO dto) throws Exception {
		return session.selectOne(namespace+".checkBidding",dto);
	}

	@Override
	public void bidding(BidDTO dto) throws Exception {
		session.insert(namespace+".bidding",dto);
	}

	@Override
	public void biddingCancel(int bid_id) throws Exception {
		session.update(namespace+".biddingCancel",bid_id);
	}

	@Override
	public List<BidVO> listAllBidding(int user_id) throws Exception {
		return session.selectList(namespace+".listAllBid",user_id);
	}

	@Override
	public List<BidVO> listAllCancelBid(int user_id) throws Exception {
		return session.selectList(namespace+".listAllCancelBid",user_id);
	}
	@Override
	public List<BidVO> listAllBidOnOption(int option_id) throws Exception {
		return session.selectList(namespace+".listAllBidOnOption",option_id);
	}

	@Override
	public int countBidOnOption(int option_id) throws Exception {
		return session.selectOne(namespace+".countBidOnOption",option_id);
	}

}
