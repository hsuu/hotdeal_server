package com.alwayke.lessonap.persistence;

import com.alwayke.lessonap.domain.*;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by hs on 2017-05-30.
 */
@Repository
public class TicketDAOImpl implements TicketDAO{

    @Inject
    private SqlSession session;
    private static String namespace = "com.alwayke.lessonap.mapper.ticketMapper";

    @Override
    public TicketVO read(int ticket_id) throws Exception {
        return session.selectOne(namespace+".read",ticket_id);
    }

    @Override
    public List<TicketOptionVO> readOptions(int ticket_id) throws Exception {
        return session.selectList(namespace+".readOptions",ticket_id);
    }

    @Override
    public int create(TicketVO vo) throws Exception {
        session.insert(namespace + ".create", vo);

        return vo.getTicket_id();
    }

    @Override
    public void createImg(ImageVO vo) throws Exception {
        session.update(namespace+".createImg",vo);
    }

    @Override
    public void createOption(TicketOptionVO vo) throws Exception {
        session.insert(namespace+".createOption",vo);
    }

    @Override
    public List<TicketVO> listAllProceed() throws Exception {
        return session.selectList(namespace + ".listAllProceed");
    }

    @Override
    public List<TicketVO> listAllComing() throws Exception {
        return session.selectList(namespace + ".listAllComing");
    }

    @Override
    public List<TicketVO> listAllEnd() throws Exception {
        return session.selectList(namespace+".listAllEnd");
    }

    @Override
    public List<TicPurchaseDTO> listAllPurchase(int tic_option_id) throws Exception {
        return session.selectList(namespace+".listAllPurchase",tic_option_id);
    }

    @Override
    public int checkPurchase(int tic_option_id) throws Exception {
        return session.selectOne(namespace+".checkPurchase",tic_option_id);
    }

    @Override
    public void purchaseTicket(TicPurchaseVO vo) throws Exception {
        session.insert(namespace+".purchaseTicket",vo);
    }

    @Override
    public void updatePurchaseFlag(int tic_purchase_id) throws Exception {
        session.update(namespace+".updatePurchaseFlag",tic_purchase_id);
    }
}
