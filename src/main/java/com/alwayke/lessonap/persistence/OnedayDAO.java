package com.alwayke.lessonap.persistence;

import com.alwayke.lessonap.domain.*;

import java.util.List;

public interface OnedayDAO {

	public OnedayVO read(int oneday_id) throws Exception;

	public List<OdyOptionVO> readOptions(int oneday_id) throws Exception;

	public int create(OnedayVO vo) throws Exception;
	public void createImg(ImageVO vo)throws Exception;

	public void createOption(OdyOptionVO vo)throws Exception;

	public List<OnedayVO> listAllProceed() throws Exception;
	public List<OnedayVO> listAllComing() throws Exception;
	public List<OnedayVO> listAllEnd() throws Exception;

	public List<OdyPurchaseDTO> listAllPurchase(int ody_option_id) throws Exception;
	public int checkPurchase(int ody_option_id)throws Exception;
	public void purchaseOneday(OdyPurchaseVO vo)throws Exception;
	public void updatePurchaseFlag(int ody_purchase_id)throws Exception;

}
